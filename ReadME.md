#2081030
	使用 spring-boot-starter-actuator 控制端点：/loggers，查询修改日志级别。
		1、pom.xml 引入针对  spring-boot-starter-actuator 的支持；
		
		2、application.properties 增加运行状态 actuator监控：
			endpoints.loggers.enabled=true
			endpoints.loggers.sensitive=false
			management.context-path=/actuator
			management.port=28779
			
		3、测试：
			3.1、浏览器发送  GET请求(http://127.0.0.1:28779/actuator/loggers), 查看所有的日志级别；
			3.2、发送 POST 请求(/manage/loggers/{elephant})	{elephant}为前面查询到的目录
				发送如下请求报文，请求发送完毕之后，再次发送如上GET 请求，即可看到对应的日志级别已经发生了改变
					{
					    "configuredLevel": "ERROR"
					}
					
#20181101
	修改打包方式：	JAR---> WAR
	
		1、修改pom.xml ，<packaging>war</packaging> 
		
		2、修改pom.xml 增加如下maven坐标(打包后隔离内置的tomcat部分)：
			    <dependency>
		            <groupId>org.springframework.boot</groupId>
		            <artifactId>spring-boot-starter-tomcat</artifactId>
		            <scope>provided</scope>
			    </dependency>
			    <dependency>
				    <groupId>javax.servlet</groupId>
				    <artifactId>javax.servlet-api</artifactId>
				    <version>3.1.0</version>
				    <scope>provided</scope>
				</dependency>
				
		3、与启动类同级，增加 PapStarterApplication 类文件，详见代码部分
		
		4、使用   mvn clean package 进行打包 war ，部署到容器中即可；

#20181129
    SpringBoot中解析器的使用
        新增 @LoginedUser 注解 与 LoginedUserArgumentResolver 参数解析器
        可以做到从  request.getHeader("TOKEN"); 获取到登录用户的所有信息进行返回；
        测试部分可见 DictController.loginedUser(...)
        
#20181129
    Fegin 的支持处理
        1、pom.xml 增加节点：
                <dependency>
                    <groupId>org.springframework.cloud</groupId>
                    <artifactId>spring-cloud-starter-feign</artifactId>
                </dependency>
                
        2、启动类部分增加 @EnableFeignClients 注解，开启 fegin 的支持
        
        3、增加接口类文件，建议与 服务提供者的方法参数保持一致， 其中对应的DTO 的返回值，在当前的服务中进行拷贝处理。
            
            @FeignClient(value = "datas-producer",fallback=DictAgentFeignRemoteHystrix.class, configuration = FeignConfiguration.class)
            public interface DictAgentFeign {
                /**
                 * 参数校验，增加自定义参数校验部分(注解)
                 *	入参校验测试处理.
                 * @return 成功的信息or失败信息(含参数校验失败部分)
                 */
                @RequestMapping(value = "/v1/data/dict/selectinfobycode/{code}")
                public ResponseVO<DictDTO> selectinfobyCode(@RequestHeader(value = "clientLicenseId", defaultValue = "-1") String clientLicenseId,
                                                            @RequestHeader(value = "loginUserId", defaultValue = "-1") String loginUserId,
                                                            @PathVariable(value = "code") String code) throws Exception;
            
            
                /**
                 * 优雅的获取登录用户信息
                 * @param loginedUserVO	当前登录用户的VO类
                 * @return
                 */
                @RequestMapping(path = "/v1/data/dict/logineduser")
                public ResponseVO loginedUser(LoginedUserVO loginedUserVO);
            
                @RequestMapping(value = "/v1/data/dict/selectfeginbyIds/{id}")
                public ResponseVO selectFeginByIds(@PathVariable(value = "id") String id) throws Exception;
            }
            
            /*
             * 熔断处理
             */
            @Component
            class DictAgentFeignRemoteHystrix implements DictAgentFeign{
            
                @Override
                public ResponseVO selectinfobyCode(@RequestHeader(value = "clientLicenseId", defaultValue = "-1") String clientLicenseId,
                                                   @RequestHeader(value = "loginUserId", defaultValue = "-1") String loginUserId,
                                                   @PathVariable(value = "code") String code) {
                    return ResponseVO.exceptiondata("超时");
                }
            
                @Override
                public ResponseVO loginedUser(LoginedUserVO loginedUserVO) {
                    return ResponseVO.exceptiondata("超时");
                }
            
                @Override
                public ResponseVO selectFeginByIds(@PathVariable(value = "id") String id) throws Exception {
                    return ResponseVO.exceptiondata(id + "超时");
                }
            }
            
            
            @Configuration
            public class FeignConfiguration implements RequestInterceptor {
            
                /**
                 * 转发header 部分的数据，保持请求头数据完整
                 * @param template
                 */
                @Override
                public void apply(RequestTemplate template) {
                    ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
                            .getRequestAttributes();
                    HttpServletRequest request = attributes.getRequest();
                    Enumeration<String> headerNames = request.getHeaderNames();
                    if (headerNames != null) {
                        while (headerNames.hasMoreElements()) {
                            String name = headerNames.nextElement();
                            String values = request.getHeader(name);
                            template.header(name, values);
            
                        }
                    }
                }
            }
        4、 熔断开关的手工开启 feign.hystrix.enabled=true
        
#20200924

    闭包表 解决树形结构存储问题，需开发测试。
    Closure Table mybatis move
    https://github.com/applicationh/Closure-Table-ClosureTable
    
    https://github.com/Kaciras/ClosureTableCateogryStore
    

#20200928
    慎用 insert into A select * from B
    
    在默认的事务隔离级别下：insert into order_record select * from order_today 加锁规则是：order_record 表锁，order_today逐步锁（扫描一个锁一个）。
    
    通过观察迁移sql的执行情况你会发现order_today是全表扫描，也就意味着在执行insert into select from 语句时，mysql会从上到下扫描order_today内的记录并且加锁，这样一来不就和直接锁表是一样了。
    这也就可以解释，为什么会逐步出现异常，慢慢开始出错。
            因为一开始只锁定了少部分数据，没有被锁定的数据还是可以正常被修改为正常状态。
            由于锁定的数据越来越多，就导致出现了大量支付失败。最后全部锁住，导致无法插入订单，而出现初始化订单失败。
            
    由于查询条件会导致order_today全表扫描，什么能避免全表扫描呢。 很简单嘛，给pay_success_time字段添加一个idx_pay_suc_time索引就可以了。由于走索引查询，就不会出现扫描全表的情况而锁表了，只会锁定符合条件的记录。 
        最终的sql
        INSERT INTO order_record SELECT *  FROM order_today FORCE INDEX (idx_pay_suc_time)    
        WHERE pay_success_time <= '2020-03-08 00:00:00';    
    
#20201106
    修改 controller 类下面的方法名称，确保整个项目下名称不重复，避免 swagger-ui 在扫描的过程中，出现同名方法。
    
    问题复现： 项目在启动过程中，会出现形如：
        Generating unique operation named：delUsingPOST_1
        Generating unique operation named：sortedUsingPOST_1
        Generating unique operation named：formUsingGET_1
        Generating unique operation named：listUsingGET_1
    的日志，其原因有两个方面，
        1、在controller上加了@RequestMapping注解后,在方法上mapping注解，需要指定为具体的请求方式，形如 post/get/delete;
        2、相同类中定义的方法名称一致(比如controller类)
