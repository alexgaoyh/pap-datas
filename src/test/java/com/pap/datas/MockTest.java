package com.pap.datas;

import static org.mockito.Mockito.when;

import com.pap.datas.dict.service.IDictService;
import com.pap.datas.dto.DictDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 微服务下，开发中往往出现A应用中某功能的实现需要调用B应用的接口,
 * 无论使用RPC还是restful都需要B应用提供接口的实现整个开发工作才能继续进行。
 * 从而导致A应用的开发停滞，整个系统的开发效率降低。
 * 这时该mock出场了。通过模拟一个接口的实现，让A应用假设能够正常调用B应用，并得到相应的返回值或产生指定类型的异常。
 * @author alexgaoyh
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PapDatasApplication.class)
public class MockTest {

	@Mock
	private IDictService dictService;

	@Before
	public void setUp() {
		DictDTO mockDict = new DictDTO();
		mockDict.setDictName("ALEXGAOYH");
		when(dictService.selectDictWithDetailInfoByCode("", "")).thenReturn(mockDict);
	}

	@Test
	public void selectDictWithDetailInfoByCodeTest() {
		DictDTO mockDict = dictService.selectDictWithDetailInfoByCode("", "");
		Assert.assertEquals(mockDict.getDictName(), "ALEXGAOYH");
	}
	
}
