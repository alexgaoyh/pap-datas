package com.pap.datas;

import com.pap.datas.dict.entity.Dict;
import com.pap.datas.dict.service.IDictService;
import com.pap.datas.dto.DictDTO;
import com.pap.datas.dto.DictDetailDTO;
import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PapDatasApplication.class)
public class PapDatasApplicationTests {

	@Test
	public void contextLoads() {
		
	}
	
	@Autowired
	private IDictService dictService;
	
	/**
	 * Junit 单元测试事务自动回滚
	 */
	// @Test
	@Transactional
	@Rollback(true)// 事务自动回滚，默认是true。可以不写
	public void createDict(){
		DictDTO dto = new DictDTO();
		dto.setDictName("1");
		List<DictDetailDTO> details = new ArrayList<DictDetailDTO>();
		DictDetailDTO temp = new DictDetailDTO();
		temp.setDictDetailName("1-name");
		details.add(temp);
		dto.setDetails(details);

		dictService.saveDictWithDetailInfo(dto);
	}

	//引入 ContiPerf 进行性能测试
	@Rule
	public ContiPerfRule contiPerfRule = new ContiPerfRule();

	// @Test
//	@PerfTest(invocations = 300)：执行300次，和线程数量无关，默认值为1，表示执行1次；
//	@PerfTest(threads=30)：并发执行30个线程，默认值为1个线程；
//	@PerfTest(duration = 20000)：重复地执行测试至少执行20s。
	@PerfTest(invocations = 100,threads = 10)
	public void prefTest() {
		List<Dict> dictList = dictService.selectListByMap(null);
		System.out.println(dictList.size());
	}

}
