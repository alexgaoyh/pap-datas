package com.pap.datas.dict.service;

import com.pap.datas.dict.entity.DictDetail;

import java.util.List;
import java.util.Map;

public interface IDictDetailService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<DictDetail> selectListByMap(Map<Object, Object> map);

    int insert(DictDetail record);

    int insertSelective(DictDetail record);

    DictDetail selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DictDetail record);

    int updateByPrimaryKey(DictDetail record);

    // alexgaoyh added begin

    /**
     * 根据 字典编码 查询出来字典明细的编号和名称
     * @param dictCode
     * @return
     */
    Map<String, String> selectDictDetailIdAndNameByDictCode(String dictCode);


    /**
     * 根据 字典编码 查询出来字典明细的编号和名称
     * @param dictCode
     * @return
     */
    Map<String, String> selectDictDetailNameAndIdByDictCode(String dictCode);

    /**
     * 根据 字典明细编号，查询 字典名称名称
     * @param dictDetailId
     * @return
     */
    String selectDictDetailNameByDictDetailId(String dictDetailId);

}
