package com.pap.datas.dict.service.impl;

import com.pap.base.util.string.StringUtilss;
import com.pap.datas.dict.entity.DictDetail;
import com.pap.datas.dict.mapper.DictDetailMapper;
import com.pap.datas.dict.service.IDictDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("dictDetailService")
public class DictDetailServiceImpl implements IDictDetailService {

    @Resource(name = "dictDetailMapper")
    private DictDetailMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<DictDetail> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(DictDetail record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(DictDetail record) {
       return mapper.insertSelective(record);
    }

    @Override
    public DictDetail selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(DictDetail record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(DictDetail record) {
      return mapper.updateByPrimaryKey(record);
    }

    // alexgaoyh added begin

    @Override
    public Map<String, String> selectDictDetailIdAndNameByDictCode(String dictCode) {
        Map<String, String> returnMap = new HashMap<>();

        List<Map<String, String>> mapList = mapper.selectDictDetailIdAndNameByDictCode(dictCode);
        if(mapList != null && mapList.size() > 0) {
            for(Map<String, String> tmp : mapList) {
                returnMap.put(tmp.get("id"), tmp.get("name"));
            }
        }

        return returnMap;
    }

    @Override
    public Map<String, String> selectDictDetailNameAndIdByDictCode(String dictCode) {
        Map<String, String> returnMap = new HashMap<>();

        List<Map<String, String>> mapList = mapper.selectDictDetailIdAndNameByDictCode(dictCode);
        if(mapList != null && mapList.size() > 0) {
            for(Map<String, String> tmp : mapList) {
                returnMap.put(tmp.get("name"), tmp.get("id"));
            }
        }

        return returnMap;
    }

    @Override
    public String selectDictDetailNameByDictDetailId(String dictDetailId) {
        DictDetail dictDetail = mapper.selectByPrimaryKey(dictDetailId);
        if(dictDetail != null && StringUtilss.isNotEmpty(dictDetail.getDictDetailId())) {
            return dictDetail.getDictDetailName();
        }
        return null;
    }
}