package com.pap.datas.dict.service;

import com.pap.datas.dict.entity.Dict;
import com.pap.datas.dto.DictDTO;

import java.util.List;
import java.util.Map;

public interface IDictService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<Dict> selectListByMap(Map<Object, Object> map);

    int insert(Dict record);

    int insertSelective(Dict record);

    Dict selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Dict record);

    int updateByPrimaryKey(Dict record);

    /**
     * 保存字典明细相关数据
     * @param operObj
     * @return
     */
    String saveDictWithDetailInfo(DictDTO operObj);

    /**
     * 更新字典明细相关数据
     * @param operObj
     * @return
     */
    String updateDictWithDetailInfo(DictDTO operObj);

    /**
     * 删除字典明细相关数据
     * @param operObj
     * @return
     */
    int deleteDictWithDetailInfo(DictDTO operObj);

    /**
     * 查询字典明细相关数据
     * @param inputSelectDictId
     * @return
     */
    DictDTO selectDictWithDetailInfoById(String inputSelectDictId, String clientLicenseId);

    /**
     * 查询字典明细相关数据
     * @param inputSelectDictCode
     * @return
     */
    DictDTO selectDictWithDetailInfoByCode(String inputSelectDictCode, String clientLicenseId);

}
