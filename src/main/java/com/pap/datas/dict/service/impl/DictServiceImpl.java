package com.pap.datas.dict.service.impl;

import com.github.stuxuhai.jpinyin.PinyinException;
import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.date.DateUtils;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.datas.dict.entity.Dict;
import com.pap.datas.dict.entity.DictDetail;
import com.pap.datas.dict.mapper.DictDetailMapper;
import com.pap.datas.dict.mapper.DictMapper;
import com.pap.datas.dict.service.IDictService;
import com.pap.datas.dto.DictDTO;
import com.pap.datas.dto.DictDetailDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("dictService")
public class DictServiceImpl implements IDictService {

    @Resource(name = "dictMapper")
    private DictMapper mapper;

    @Resource(name = "dictDetailMapper")
    private DictDetailMapper dictDetailMapper;

    @Resource(name = "idWorker")
    private IdWorker idWorker;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<Dict> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(Dict record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(Dict record) {
       return mapper.insertSelective(record);
    }

    @Override
    public Dict selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Dict record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Dict record) {
      return mapper.updateByPrimaryKey(record);
    }


    @Override
    public String saveDictWithDetailInfo(DictDTO operObj) {
        String currentOperDictId = idWorker.nextIdStr();
        if(operObj != null) {

            // BeanCopy
            com.pap.datas.dict.entity.Dict databaseDict = new com.pap.datas.dict.entity.Dict();
            BeanCopyUtilss.myCopyProperties(operObj, databaseDict);

            List<DictDetail> databaseDictDetailList = new ArrayList<DictDetail>();
            List<DictDetailDTO> inputDetailList = operObj.getDetails();
            if(inputDetailList != null) {
                databaseDictDetailList = toDetailDB(inputDetailList);
            }

            databaseDict.setDictId(currentOperDictId);
            if(StringUtilss.isEmpty(operObj.getDictCode())) {
                try {
                    databaseDict.setDictCode(PinyinHelper.convertToPinyinString(databaseDict.getDictName(), "", PinyinFormat.WITHOUT_TONE));
                } catch (PinyinException e) {
                    e.printStackTrace();
                }
            }
            databaseDict.setCreateIp("0.0.0.0");
            databaseDict.setCreateTime(DateUtils.getCurrDateTimeStr());
            mapper.insertSelective(databaseDict);
            if(databaseDictDetailList != null) {
                for (DictDetail detailDTO : databaseDictDetailList) {
                    detailDTO.setDictDetailId(idWorker.nextIdStr());
                    detailDTO.setDictId(currentOperDictId);
                    detailDTO.setDictDetailCode(databaseDict.getDictCode());
                    detailDTO.setDictDetailName(databaseDict.getDictName());
                    detailDTO.setCreateIp(databaseDict.getCreateIp());
                    detailDTO.setCreateUser(databaseDict.getCreateUser());
                    detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                    detailDTO.setClientLicenseId(databaseDict.getClientLicenseId());
                    dictDetailMapper.insertSelective(detailDTO);
                }
            }
        }
        return currentOperDictId;
    }

    @Override
    public String updateDictWithDetailInfo(DictDTO operObj) {
        if(operObj != null) {
            // BeanCopy
            com.pap.datas.dict.entity.Dict databaseDict = new com.pap.datas.dict.entity.Dict();
            BeanCopyUtilss.myCopyProperties(operObj, databaseDict);

            List<DictDetail> databaseDictDetailList = new ArrayList<DictDetail>();
            List<DictDetailDTO> inputDetailList = operObj.getDetails();
            if(inputDetailList != null) {
                databaseDictDetailList = toDetailDB(inputDetailList);
            }

            mapper.updateByPrimaryKeySelective(databaseDict);

            if(databaseDictDetailList != null) {
                // 删除已被删除的明细数据: dto中无明细数据，数据库中有明细数据，删除
                Map<Object, Object> operSelectDictMap = new HashMap<Object, Object>();
                operSelectDictMap.put("dictId", databaseDict.getDictId());
                List<DictDetail> dbDictDetailList = dictDetailMapper.selectListByMap(operSelectDictMap);
                if(dbDictDetailList != null && dbDictDetailList.size() > 0) {
                    for(DictDetail dbDictDetail : dbDictDetailList) {
                        boolean existBool = false;
                        for(DictDetailDTO dictDetailDTO: inputDetailList) {
                            if(dbDictDetail.getDictDetailId().equals(dictDetailDTO.getDictDetailId())) {
                                existBool = true;
                            }
                        }
                        // existBool = true 说明 dbDictDetail 数据未被删除
                        if(existBool == false) {
                            dictDetailMapper.deleteByPrimaryKey(dbDictDetail.getDictDetailId());
                        }

                    }
                }


                for (DictDetail detailDTO : databaseDictDetailList) {
                    DictDetail temp = dictDetailMapper.selectByPrimaryKey(detailDTO.getDictDetailId());
                    if(temp != null) {
                        detailDTO.setDictDetailCode(databaseDict.getDictCode());
                        detailDTO.setDictDetailName(databaseDict.getDictName());
                        detailDTO.setModifyIp(operObj.getModifyIp());
                        detailDTO.setModifyUser(operObj.getModifyUser());
                        detailDTO.setModifyTime(DateUtils.getCurrDateTimeStr());
                        dictDetailMapper.updateByPrimaryKeySelective(detailDTO);
                    } else {
                        detailDTO.setDictDetailId(idWorker.nextIdStr());
                        detailDTO.setDictId(operObj.getDictId());
                        detailDTO.setDictDetailCode(databaseDict.getDictCode());
                        detailDTO.setDictDetailName(databaseDict.getDictName());
                        detailDTO.setCreateIp(operObj.getCreateIp());
                        detailDTO.setCreateUser(operObj.getCreateUser());
                        detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                        detailDTO.setClientLicenseId(databaseDict.getClientLicenseId());
                        dictDetailMapper.insertSelective(detailDTO);
                    }
                }
            }
        }
        return operObj.getDictId();
    }

    @Override
    public int deleteDictWithDetailInfo(DictDTO operObj) {
        String inputDictId = operObj.getDictId();
        mapper.deleteByPrimaryKey(inputDictId);
        dictDetailMapper.deleteByDictId(inputDictId);
        return 0;
    }

    @Override
    public DictDTO selectDictWithDetailInfoById(String inputSelectDictId, String clientLicenseId) {
        DictDTO dictDTO = new DictDTO();
        List<DictDetailDTO> dictDetailDTOList = new ArrayList<DictDetailDTO>();
        if(StringUtilss.isNotEmpty(inputSelectDictId) && StringUtilss.isNotEmpty(clientLicenseId)) {

            com.pap.datas.dict.entity.Dict databaseDict = mapper.selectByPrimaryKey(inputSelectDictId);
            if(databaseDict != null) {
                BeanCopyUtilss.myCopyProperties(databaseDict, dictDTO);
            }


            Map<Object, Object> operSelectDictMap = new HashMap<Object, Object>();
            operSelectDictMap.put("dictId", inputSelectDictId);
            operSelectDictMap.put("clientLicenseId", clientLicenseId);
            List<DictDetail> databaseDictDetailList = dictDetailMapper.selectListByMap(operSelectDictMap);
            if(databaseDictDetailList != null) {
                dictDetailDTOList = toDetailDTO(databaseDictDetailList);
            }

        }
        dictDTO.setDetails(dictDetailDTOList);

        return dictDTO;
    }

    @Override
    public DictDTO selectDictWithDetailInfoByCode(String inputSelectDictCode, String clientLicenseId) {
        DictDTO dictDTO = new DictDTO();
        List<DictDetailDTO> dictDetailDTOList = new ArrayList<DictDetailDTO>();
        if(StringUtilss.isNotEmpty(inputSelectDictCode) && StringUtilss.isNotEmpty(clientLicenseId)) {

            Map<Object, Object> operSelectDictMap = new HashMap<Object, Object>();
            operSelectDictMap.put("dictCode", inputSelectDictCode);
            operSelectDictMap.put("clientLicenseId", clientLicenseId);

            List<com.pap.datas.dict.entity.Dict> databaseDictList = mapper.selectListByMap(operSelectDictMap);
            if(databaseDictList != null && databaseDictList.size() > 0) {
                com.pap.datas.dict.entity.Dict databaseDict = databaseDictList.get(0);
                BeanCopyUtilss.myCopyProperties(databaseDict, dictDTO);
            }


            Map<Object, Object> operSelectDictDetailMap = new HashMap<Object, Object>();
            operSelectDictDetailMap.put("dictId", dictDTO.getDictId());
            List<DictDetail> databaseDictDetailList = dictDetailMapper.selectListByMap(operSelectDictDetailMap);
            if(databaseDictDetailList != null) {
                dictDetailDTOList = toDetailDTO(databaseDictDetailList);
            }
        }
        dictDTO.setDetails(dictDetailDTOList);

        return dictDTO;
    }

    private List<DictDTO> toDTO(List<Dict> databaseList) {
        List<DictDTO> returnList = new ArrayList<DictDTO>();
        if(databaseList != null) {
            for(Dict dbEntity : databaseList) {
                DictDTO dtoTemp = new DictDTO();
                BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
                returnList.add(dtoTemp);
            }
        }
        return returnList;
    }

    private List<DictDetailDTO> toDetailDTO(List<DictDetail> databaseList) {
        List<DictDetailDTO> returnList = new ArrayList<DictDetailDTO>();
        if(databaseList != null) {
            for(DictDetail dbEntity : databaseList) {
                DictDetailDTO dtoTemp = new DictDetailDTO();
                BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
                returnList.add(dtoTemp);
            }
        }
        return returnList;
    }

    private List<Dict> toDB(List<DictDTO> dtoList) {
        List<Dict> returnList = new ArrayList<Dict>();
        if(dtoList != null) {
            for(DictDTO dtoEntity : dtoList) {
                Dict dbTemp = new Dict();
                BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
                returnList.add(dbTemp);
            }
        }
        return returnList;
    }

    private List<DictDetail> toDetailDB(List<DictDetailDTO> dtoList) {
        List<DictDetail> returnList = new ArrayList<DictDetail>();
        if(dtoList != null) {
            for(DictDetailDTO dtoEntity : dtoList) {
                DictDetail dbTemp = new DictDetail();
                BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
                returnList.add(dbTemp);
            }
        }
        return returnList;
    }
}