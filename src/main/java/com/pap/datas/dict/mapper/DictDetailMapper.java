package com.pap.datas.dict.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.datas.dict.entity.DictDetail;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface DictDetailMapper extends PapBaseMapper<DictDetail> {
    int deleteByPrimaryKey(String dictDetailId);

    int selectCountByMap(Map<Object, Object> map);

    List<DictDetail> selectListByMap(Map<Object, Object> map);

    DictDetail selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(DictDetail record);

    int insertSelective(DictDetail record);

    DictDetail selectByPrimaryKey(String dictDetailId);

    int updateByPrimaryKeySelective(DictDetail record);

    int updateByPrimaryKey(DictDetail record);

    // alexgaoyh add
    int deleteByDictId(@Param("dictId") String dictId);

    /**
     * 根据 字典编码 查询出来字典明细的编号和名称
     * @param dictCode
     * @return
     */
    List<Map<String, String>> selectDictDetailIdAndNameByDictCode(@Param("dictCode") String dictCode);
}