package com.pap.datas.dict.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.datas.dict.entity.Dict;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface DictMapper extends PapBaseMapper<Dict> {
    int deleteByPrimaryKey(String dictId);

    int selectCountByMap(Map<Object, Object> map);

    List<Dict> selectListByMap(Map<Object, Object> map);

    Dict selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(Dict record);

    int insertSelective(Dict record);

    Dict selectByPrimaryKey(String dictId);

    int updateByPrimaryKeySelective(Dict record);

    int updateByPrimaryKey(Dict record);
}