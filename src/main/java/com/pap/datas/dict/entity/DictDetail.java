package com.pap.datas.dict.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_data_dict_detail", namespace = "com.pap.datas.dict.mapper.DictDetailMapper", remarks = " 修改点 ", aliasName = "t_data_dict_detail t_data_dict_detail" )
public class DictDetail extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_data_dict_detail.DICT__DETAIL_ID
     */
    @MyBatisColumnAnnotation(name = "DICT__DETAIL_ID", value = "t_data_dict_detail_DICT__DETAIL_ID", chineseNote = "编号", tableAlias = "t_data_dict_detail", orderNo = 1, methodName = "dictDetailId")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String dictDetailId;

    /**
     *  编码,所属表字段为t_data_dict_detail.DICT__DETAIL_CODE
     */
    @MyBatisColumnAnnotation(name = "DICT__DETAIL_CODE", value = "t_data_dict_detail_DICT__DETAIL_CODE", chineseNote = "编码", tableAlias = "t_data_dict_detail", orderNo = 2, methodName = "dictDetailCode")
    @MyApiModelPropertyAnnotation(value = "编码")
    private String dictDetailCode;

    /**
     *  名称,所属表字段为t_data_dict_detail.DICT__DETAIL_NAME
     */
    @MyBatisColumnAnnotation(name = "DICT__DETAIL_NAME", value = "t_data_dict_detail_DICT__DETAIL_NAME", chineseNote = "名称", tableAlias = "t_data_dict_detail", orderNo = 3, methodName = "dictDetailName")
    @MyApiModelPropertyAnnotation(value = "名称")
    private String dictDetailName;

    /**
     *  所属字典编号,所属表字段为t_data_dict_detail.DICT_ID
     */
    @MyBatisColumnAnnotation(name = "DICT_ID", value = "t_data_dict_detail_DICT_ID", chineseNote = "所属字典编号", tableAlias = "t_data_dict_detail", orderNo = 4, methodName = "dictId")
    @MyApiModelPropertyAnnotation(value = "所属字典编号")
    private String dictId;

    /**
     *  所属字典编码,所属表字段为t_data_dict_detail.DICT_CODE
     */
    @MyBatisColumnAnnotation(name = "DICT_CODE", value = "t_data_dict_detail_DICT_CODE", chineseNote = "所属字典编码", tableAlias = "t_data_dict_detail", orderNo = 5, methodName = "dictCode")
    @MyApiModelPropertyAnnotation(value = "所属字典编码")
    private String dictCode;

    /**
     *  所属字典名称,所属表字段为t_data_dict_detail.DICT_NAME
     */
    @MyBatisColumnAnnotation(name = "DICT_NAME", value = "t_data_dict_detail_DICT_NAME", chineseNote = "所属字典名称", tableAlias = "t_data_dict_detail", orderNo = 6, methodName = "dictName")
    @MyApiModelPropertyAnnotation(value = "所属字典名称")
    private String dictName;

    /**
     *  数据值,所属表字段为t_data_dict_detail.NUM_VALUE
     */
    @MyBatisColumnAnnotation(name = "NUM_VALUE", value = "t_data_dict_detail_NUM_VALUE", chineseNote = "数据值", tableAlias = "t_data_dict_detail", orderNo = 7, methodName = "numValue")
    @MyApiModelPropertyAnnotation(value = "数据值")
    private String numValue;

    /**
     *  字符值,所属表字段为t_data_dict_detail.STR_VALUE
     */
    @MyBatisColumnAnnotation(name = "STR_VALUE", value = "t_data_dict_detail_STR_VALUE", chineseNote = "字符值", tableAlias = "t_data_dict_detail", orderNo = 8, methodName = "strValue")
    @MyApiModelPropertyAnnotation(value = "字符值")
    private String strValue;

    /**
     *  备注,所属表字段为t_data_dict_detail.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_data_dict_detail_REMARK", chineseNote = "备注", tableAlias = "t_data_dict_detail", orderNo = 9, methodName = "remark")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    @Override
    public String getDynamicTableName() {
        return "t_data_dict_detail";
    }

    private static final long serialVersionUID = 1L;

    public String getDictDetailId() {
        return dictDetailId;
    }

    public void setDictDetailId(String dictDetailId) {
        this.dictDetailId = dictDetailId;
    }

    public String getDictDetailCode() {
        return dictDetailCode;
    }

    public void setDictDetailCode(String dictDetailCode) {
        this.dictDetailCode = dictDetailCode;
    }

    public String getDictDetailName() {
        return dictDetailName;
    }

    public void setDictDetailName(String dictDetailName) {
        this.dictDetailName = dictDetailName;
    }

    public String getDictId() {
        return dictId;
    }

    public void setDictId(String dictId) {
        this.dictId = dictId;
    }

    public String getDictCode() {
        return dictCode;
    }

    public void setDictCode(String dictCode) {
        this.dictCode = dictCode;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public String getNumValue() {
        return numValue;
    }

    public void setNumValue(String numValue) {
        this.numValue = numValue;
    }

    public String getStrValue() {
        return strValue;
    }

    public void setStrValue(String strValue) {
        this.strValue = strValue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dictDetailId=").append(dictDetailId);
        sb.append(", dictDetailCode=").append(dictDetailCode);
        sb.append(", dictDetailName=").append(dictDetailName);
        sb.append(", dictId=").append(dictId);
        sb.append(", dictCode=").append(dictCode);
        sb.append(", dictName=").append(dictName);
        sb.append(", numValue=").append(numValue);
        sb.append(", strValue=").append(strValue);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }
}