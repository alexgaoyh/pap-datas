package com.pap.datas.dict.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_data_dict", namespace = "com.pap.datas.dict.mapper.DictMapper", remarks = " 修改点 ", aliasName = "t_data_dict t_data_dict" )
public class Dict extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_data_dict.DICT_ID
     */
    @MyBatisColumnAnnotation(name = "DICT_ID", value = "t_data_dict_DICT_ID", chineseNote = "编号", tableAlias = "t_data_dict")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String dictId;

    /**
     *  编码,所属表字段为t_data_dict.DICT_CODE
     */
    @MyBatisColumnAnnotation(name = "DICT_CODE", value = "t_data_dict_DICT_CODE", chineseNote = "编码", tableAlias = "t_data_dict")
    @MyApiModelPropertyAnnotation(value = "编码")
    private String dictCode;

    /**
     *  名称,所属表字段为t_data_dict.DICT_NAME
     */
    @MyBatisColumnAnnotation(name = "DICT_NAME", value = "t_data_dict_DICT_NAME", chineseNote = "名称", tableAlias = "t_data_dict")
    @MyApiModelPropertyAnnotation(value = "名称")
    private String dictName;

    /**
     *  备注,所属表字段为t_data_dict.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_data_dict_REMARK", chineseNote = "备注", tableAlias = "t_data_dict")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    /**
     *  排序号,所属表字段为t_data_dict.ORDER_NO
     */
    @MyBatisColumnAnnotation(name = "ORDER_NO", value = "t_data_dict_ORDER_NO", chineseNote = "排序号", tableAlias = "t_data_dict")
    @MyApiModelPropertyAnnotation(value = "排序号")
    private String orderNo;

    /**
     *  字典类型,所属表字段为t_data_dict.DICT_TYPE
     */
    @MyBatisColumnAnnotation(name = "DICT_TYPE", value = "t_data_dict_DICT_TYPE", chineseNote = "字典类型", tableAlias = "t_data_dict")
    @MyApiModelPropertyAnnotation(value = "字典类型")
    private String dictType;

    @Override
    public String getDynamicTableName() {
        return "t_data_dict";
    }

    private static final long serialVersionUID = 1L;

    public String getDictId() {
        return dictId;
    }

    public void setDictId(String dictId) {
        this.dictId = dictId;
    }

    public String getDictCode() {
        return dictCode;
    }

    public void setDictCode(String dictCode) {
        this.dictCode = dictCode;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getDictType() {
        return dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dictId=").append(dictId);
        sb.append(", dictCode=").append(dictCode);
        sb.append(", dictName=").append(dictName);
        sb.append(", remark=").append(remark);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", dictType=").append(dictType);
        sb.append("]");
        return sb.toString();
    }
}