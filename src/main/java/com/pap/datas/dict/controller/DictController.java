package com.pap.datas.dict.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.validation.group.SaveGroup;
import com.pap.base.validation.group.UpdateGroup;
import com.pap.datas.atlas.entity.AtlasDetail;
import com.pap.datas.dict.entity.Dict;
import com.pap.datas.dict.entity.DictDetail;
import com.pap.datas.dict.service.IDictDetailService;
import com.pap.datas.dict.service.IDictService;
import com.pap.datas.dto.DictDTO;
import com.pap.datas.dto.vo.DictVO;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dict")
@Api(value="字典操作接口",tags={"字典操作接口"})
public class DictController {

	@Resource(name = "dictService")
	private IDictService service;

	@Resource(name = "dictDetailService")
	private IDictDetailService dictDetailService;

	@ApiOperation(value="保存", notes="保存字典信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "dictDTO", value = "字典信息" , required = true, paramType = "body", dataType = "DictDTO")
	})
	@PostMapping(value = "")
	public ResponseVO<DictDTO> dictCreate(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
										 @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
										 @RequestBody @Validated({SaveGroup.class}) DictDTO dictDTO,
									  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {

		BeanCopyUtilss.myCopyPropertiesForLoginedUser(dictDTO, loginedUserVO, true);
		String operInt = service.saveDictWithDetailInfo(dictDTO);

		DictDTO operDictDTO = service.selectDictWithDetailInfoById(operInt, loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");
		return ResponseVO.successdata(operDictDTO);
	}

	@ApiOperation(value="更新", notes="更新字典信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "dictDTO", value = "字典信息" , required = true, paramType = "body", dataType = "DictDTO")
	})
	@PutMapping(value = "")
	public ResponseVO<DictDTO> dictModify(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
								   @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
								   @RequestBody @Validated({UpdateGroup.class}) DictDTO dictDTO,
									  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {

		BeanCopyUtilss.myCopyPropertiesForLoginedUser(dictDTO, loginedUserVO, false);
		String operInt = service.updateDictWithDetailInfo(dictDTO);

		DictDTO operDictDTO = service.selectDictWithDetailInfoById(operInt, loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");
		return ResponseVO.successdata(operDictDTO);
	}

	@ApiOperation(value="删除字典信息", notes="删除字典信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "dictDTO", value = "字典信息" , required = true, paramType = "body", dataType = "DictDTO")
	})
	@SuppressWarnings("unchecked")
	@DeleteMapping(value = "")
	public ResponseVO<Dict> dictDelete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
								   @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
								   @RequestBody DictDTO dictDTO,
								   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		int operInt = service.deleteDictWithDetailInfo(dictDTO);

		return ResponseVO.successdata(operInt);
	}

	@ApiOperation(value="根据编号查询字典信息", notes="根据编号查询字典信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "dictId", value = "字典信息" , required = true, paramType = "path", dataType = "String")
	})
	@GetMapping(value = "/selectinfobyId/{dictId}")
//	@PapFilterJSONAnnotation(type = DictDTO.class , filter="createIp,modifyIp,clientLicenseId")
//	@PapFilterJSONAnnotation(type = DictDetailDTO.class, filter="createIp,modifyIp,clientLicenseId")
	public ResponseVO<DictDTO> dictSelectInfoById(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											  @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
											  @PathVariable("dictId") String dictId,
											  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		DictDTO operDictDTO = service.selectDictWithDetailInfoById(dictId, loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");

		return ResponseVO.successdata(operDictDTO);
	}

	@ApiOperation(value="根据编号查询字典信息", notes="根据编号查询字典信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "dictCode", value = "字典信息" , required = true, paramType = "path", dataType = "String")
	})
	@GetMapping(value = "/selectinfobycode/{dictCode}")
	public ResponseVO<DictDTO> dictSelectInfoByCode(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
												@PathVariable("dictCode") String dictCode,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		DictDTO operDictDTO = service.selectDictWithDetailInfoByCode(dictCode, loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");

		return ResponseVO.successdata(operDictDTO);
	}

	@ApiOperation(value="根据编号和备注查询字典明细信息", notes="根据编号和备注查询字典明细信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "dictCode", value = "字典编号", required = true, dataType = "String", paramType="query"),
			@ApiImplicitParam(name = "remark", value = "字典明细备注", required = true, dataType = "String", paramType="query"),
	})
	@GetMapping(value = "/selectdetailinfobydetailcodeandremark")
	public ResponseVO<DictDetail> dictDetailSelectInfoByCode(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
												    @RequestParam String dictCode, @RequestParam String remark,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {

		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("dictCode", dictCode);
		map.put("remark", remark);
		List<DictDetail> dictDetailList = dictDetailService.selectListByMap(map);
		return ResponseVO.successdata(dictDetailList);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "dictDTO", value = "应用查询参数", required = false, dataType = "DictDTO", paramType="body"),
			@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
			@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
			@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<DictDTO> dictQuery(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
										 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
										 @RequestBody DictDTO dictDTO,
										 @RequestParam Integer pageNo, @RequestParam Integer pageSize,
										 @RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
										 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(dictDTO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<Dict> list = service.selectListByMap(map);

		//
		List<DictDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = service.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<DictDTO> toDTO(List<Dict> databaseList) {
		List<DictDTO> returnList = new ArrayList<DictDTO>();
		if(databaseList != null) {
			for(Dict dbEntity : databaseList) {
				DictDTO dtoTemp = new DictDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	/**
	 * 参数校验，增加自定义参数校验部分(注解)
	 *	入参校验测试处理.
	 * @param vo 前端返回过来字典信息
	 * @return 成功的信息or失败信息(含参数校验失败部分)
	 */
	@PostMapping(path = "/valid")
	public ResponseVO valid(@Valid @RequestBody DictVO vo) {
		return ResponseVO.successdata(vo);
	}


	/**
	 * 优雅的获取登录用户信息
	 * @param loginedUserVO	当前登录用户的VO类
	 * @return
	 */
	@ApiOperation(value="优雅获取用户信息", notes="优雅获取用户信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
	})
	@PostMapping(path = "/logineduser")
	public ResponseVO loginedUser(@LoginedUser LoginedUserVO loginedUserVO) {
		return ResponseVO.successdata(loginedUserVO);
	}

}
