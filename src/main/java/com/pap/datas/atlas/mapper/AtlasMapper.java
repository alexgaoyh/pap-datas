package com.pap.datas.atlas.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.datas.atlas.entity.Atlas;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AtlasMapper extends PapBaseMapper<Atlas> {
    int deleteByPrimaryKey(String atlasId);

    int selectCountByMap(Map<Object, Object> map);

    List<Atlas> selectListByMap(Map<Object, Object> map);

    Atlas selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(Atlas record);

    int insertSelective(Atlas record);

    Atlas selectByPrimaryKey(String atlasId);

    int updateByPrimaryKeySelective(Atlas record);

    int updateByPrimaryKey(Atlas record);
}