package com.pap.datas.atlas.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.datas.atlas.entity.AtlasDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AtlasDetailMapper extends PapBaseMapper<AtlasDetail> {
    int deleteByPrimaryKey(String atlasDetailId);

    int selectCountByMap(Map<Object, Object> map);

    List<AtlasDetail> selectListByMap(Map<Object, Object> map);

    AtlasDetail selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(AtlasDetail record);

    int insertSelective(AtlasDetail record);

    AtlasDetail selectByPrimaryKey(String atlasDetailId);

    int updateByPrimaryKeySelective(AtlasDetail record);

    int updateByPrimaryKey(AtlasDetail record);

    // alexgaoyh add
    int deleteByAtlasId(@Param("atlasId") String atlasId);
}