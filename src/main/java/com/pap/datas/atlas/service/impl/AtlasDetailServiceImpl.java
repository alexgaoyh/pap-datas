package com.pap.datas.atlas.service.impl;

import com.pap.datas.atlas.entity.AtlasDetail;
import com.pap.datas.atlas.mapper.AtlasDetailMapper;
import com.pap.datas.atlas.service.IAtlasDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("atlasDetailService")
public class AtlasDetailServiceImpl implements IAtlasDetailService {

    @Resource(name = "atlasDetailMapper")
    private AtlasDetailMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<AtlasDetail> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(AtlasDetail record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(AtlasDetail record) {
       return mapper.insertSelective(record);
    }

    @Override
    public AtlasDetail selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(AtlasDetail record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(AtlasDetail record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public List<AtlasDetail> selectListByAtlasId(String atlasId) {
        Map<Object, Object> atlasIdMap = new HashMap<>(1);
        atlasIdMap.put("atlasId", atlasId);
        return mapper.selectListByMap(atlasIdMap);
    }
}