package com.pap.datas.atlas.service.impl;

import com.github.stuxuhai.jpinyin.PinyinException;
import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.date.DateUtils;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.datas.atlas.entity.Atlas;
import com.pap.datas.atlas.mapper.AtlasDetailMapper;
import com.pap.datas.atlas.mapper.AtlasMapper;
import com.pap.datas.atlas.service.IAtlasService;
import com.pap.datas.atlas.entity.AtlasDetail;
import com.pap.datas.dto.AtlasDTO;
import com.pap.datas.dto.AtlasDetailDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("atlasService")
public class AtlasServiceImpl implements IAtlasService {

    @Resource(name = "atlasMapper")
    private AtlasMapper mapper;

    @Resource(name = "atlasDetailMapper")
    private AtlasDetailMapper atlasDetailMapper;

    @Resource(name = "idWorker")
    private IdWorker idWorker;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<Atlas> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(Atlas record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(Atlas record) {
       return mapper.insertSelective(record);
    }

    @Override
    public Atlas selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Atlas record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Atlas record) {
      return mapper.updateByPrimaryKey(record);
    }


    @Override
    public String saveAtlasWithDetailInfo(AtlasDTO operObj) {
        String currentOperAtlasId = idWorker.nextIdStr();
        if(operObj != null) {

            // BeanCopy
            com.pap.datas.atlas.entity.Atlas databaseAtlas = new com.pap.datas.atlas.entity.Atlas();
            BeanCopyUtilss.myCopyProperties(operObj, databaseAtlas);

            List<AtlasDetail> databaseAtlasDetailList = new ArrayList<AtlasDetail>();
            List<AtlasDetailDTO> inputDetailList = operObj.getDetails();
            if(inputDetailList != null) {
                databaseAtlasDetailList = toDetailDB(inputDetailList);
            }

            databaseAtlas.setAtlasId(currentOperAtlasId);
            if(StringUtilss.isEmpty(operObj.getAtlasCode())) {
                try {
                    databaseAtlas.setAtlasCode(PinyinHelper.convertToPinyinString(databaseAtlas.getAtlasName(), "", PinyinFormat.WITHOUT_TONE));
                } catch (PinyinException e) {
                    e.printStackTrace();
                }
            }
            databaseAtlas.setCreateIp("0.0.0.0");
            databaseAtlas.setCreateTime(DateUtils.getCurrDateTimeStr());
            mapper.insertSelective(databaseAtlas);
            if(databaseAtlasDetailList != null) {
                for (AtlasDetail detailDTO : databaseAtlasDetailList) {
                    detailDTO.setAtlasDetailId(idWorker.nextIdStr());
                    detailDTO.setAtlasId(currentOperAtlasId);
                    detailDTO.setCreateIp(databaseAtlas.getCreateIp());
                    detailDTO.setCreateUser(databaseAtlas.getCreateUser());
                    detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                    detailDTO.setClientLicenseId(databaseAtlas.getClientLicenseId());
                    atlasDetailMapper.insertSelective(detailDTO);
                }
            }
        }
        return currentOperAtlasId;
    }

    @Override
    public String updateAtlasWithDetailInfo(AtlasDTO operObj) {
        if(operObj != null) {
            // BeanCopy
            com.pap.datas.atlas.entity.Atlas databaseAtlas = new com.pap.datas.atlas.entity.Atlas();
            BeanCopyUtilss.myCopyProperties(operObj, databaseAtlas);

            List<AtlasDetail> databaseAtlasDetailList = new ArrayList<AtlasDetail>();
            List<AtlasDetailDTO> inputDetailList = operObj.getDetails();
            if(inputDetailList != null) {
                databaseAtlasDetailList = toDetailDB(inputDetailList);
            }

            mapper.updateByPrimaryKeySelective(databaseAtlas);

            if(databaseAtlasDetailList != null) {
                // 删除已被删除的明细数据: dto中无明细数据，数据库中有明细数据，删除
                Map<Object, Object> operSelectAtlasMap = new HashMap<Object, Object>();
                operSelectAtlasMap.put("atlasId", databaseAtlas.getAtlasId());
                List<AtlasDetail> dbAtlasDetailList = atlasDetailMapper.selectListByMap(operSelectAtlasMap);
                if(dbAtlasDetailList != null && dbAtlasDetailList.size() > 0) {
                    for(AtlasDetail dbAtlasDetail : dbAtlasDetailList) {
                        boolean existBool = false;
                        for(AtlasDetailDTO atlasDetailDTO: inputDetailList) {
                            if(dbAtlasDetail.getAtlasDetailId().equals(atlasDetailDTO.getAtlasDetailId())) {
                                existBool = true;
                            }
                        }
                        // existBool = true 说明 dbAtlasDetail 数据未被删除
                        if(existBool == false) {
                            atlasDetailMapper.deleteByPrimaryKey(dbAtlasDetail.getAtlasDetailId());
                        }

                    }
                }


                for (AtlasDetail detailDTO : databaseAtlasDetailList) {
                    AtlasDetail temp = atlasDetailMapper.selectByPrimaryKey(detailDTO.getAtlasDetailId());
                    if(temp != null) {
                        detailDTO.setModifyIp(operObj.getModifyIp());
                        detailDTO.setModifyUser(operObj.getModifyUser());
                        detailDTO.setModifyTime(DateUtils.getCurrDateTimeStr());
                        atlasDetailMapper.updateByPrimaryKeySelective(detailDTO);
                    } else {
                        detailDTO.setAtlasDetailId(idWorker.nextIdStr());
                        detailDTO.setAtlasId(operObj.getAtlasId());
                        detailDTO.setCreateIp(operObj.getCreateIp());
                        detailDTO.setCreateUser(operObj.getCreateUser());
                        detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                        detailDTO.setClientLicenseId(databaseAtlas.getClientLicenseId());
                        atlasDetailMapper.insertSelective(detailDTO);
                    }
                }
            }
        }
        return operObj.getAtlasId();
    }

    @Override
    public int deleteAtlasWithDetailInfo(AtlasDTO operObj) {
        String inputAtlasId = operObj.getAtlasId();
        mapper.deleteByPrimaryKey(inputAtlasId);
        atlasDetailMapper.deleteByAtlasId(inputAtlasId);
        return 0;
    }

    @Override
    public AtlasDTO selectAtlasWithDetailInfoById(String inputSelectAtlasId, String clientLicenseId) {
        AtlasDTO atlasDTO = new AtlasDTO();
        List<AtlasDetailDTO> atlasDetailDTOList = new ArrayList<AtlasDetailDTO>();

        com.pap.datas.atlas.entity.Atlas databaseAtlas = mapper.selectByPrimaryKey(inputSelectAtlasId);
        if(databaseAtlas != null) {
            BeanCopyUtilss.myCopyProperties(databaseAtlas, atlasDTO);
        }


        Map<Object, Object> operSelectAtlasMap = new HashMap<Object, Object>();
        operSelectAtlasMap.put("atlasId", inputSelectAtlasId);
        operSelectAtlasMap.put("clientLicenseId", clientLicenseId);
        List<AtlasDetail> databaseAtlasDetailList = atlasDetailMapper.selectListByMap(operSelectAtlasMap);
        if(databaseAtlasDetailList != null) {
            atlasDetailDTOList = toDetailDTO(databaseAtlasDetailList);
        }

        atlasDTO.setDetails(atlasDetailDTOList);

        return atlasDTO;
    }

    @Override
    public AtlasDTO selectAtlasWithDetailInfoByCode(String inputSelectAtlasCode, String clientLicenseId) {
        AtlasDTO atlasDTO = new AtlasDTO();
        List<AtlasDetailDTO> atlasDetailDTOList = new ArrayList<AtlasDetailDTO>();

        Map<Object, Object> operSelectAtlasMap = new HashMap<Object, Object>();
        operSelectAtlasMap.put("atlasCode", inputSelectAtlasCode);
        operSelectAtlasMap.put("clientLicenseId", clientLicenseId);

        List<com.pap.datas.atlas.entity.Atlas> databaseAtlasList = mapper.selectListByMap(operSelectAtlasMap);
        if(databaseAtlasList != null && databaseAtlasList.size() > 0) {
            com.pap.datas.atlas.entity.Atlas databaseAtlas = databaseAtlasList.get(0);
            BeanCopyUtilss.myCopyProperties(databaseAtlas, atlasDTO);
        }


        Map<Object, Object> operSelectAtlasDetailMap = new HashMap<Object, Object>();
        operSelectAtlasDetailMap.put("atlasId", atlasDTO.getAtlasId());
        List<AtlasDetail> databaseAtlasDetailList = atlasDetailMapper.selectListByMap(operSelectAtlasDetailMap);
        if(databaseAtlasDetailList != null) {
            atlasDetailDTOList = toDetailDTO(databaseAtlasDetailList);
        }

        atlasDTO.setDetails(atlasDetailDTOList);

        return atlasDTO;
    }

    private List<AtlasDTO> toDTO(List<Atlas> databaseList) {
        List<AtlasDTO> returnList = new ArrayList<AtlasDTO>();
        if(databaseList != null) {
            for(Atlas dbEntity : databaseList) {
                AtlasDTO dtoTemp = new AtlasDTO();
                BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
                returnList.add(dtoTemp);
            }
        }
        return returnList;
    }

    private List<AtlasDetailDTO> toDetailDTO(List<AtlasDetail> databaseList) {
        List<AtlasDetailDTO> returnList = new ArrayList<AtlasDetailDTO>();
        if(databaseList != null) {
            for(AtlasDetail dbEntity : databaseList) {
                AtlasDetailDTO dtoTemp = new AtlasDetailDTO();
                BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
                returnList.add(dtoTemp);
            }
        }
        return returnList;
    }

    private List<Atlas> toDB(List<AtlasDTO> dtoList) {
        List<Atlas> returnList = new ArrayList<Atlas>();
        if(dtoList != null) {
            for(AtlasDTO dtoEntity : dtoList) {
                Atlas dbTemp = new Atlas();
                BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
                returnList.add(dbTemp);
            }
        }
        return returnList;
    }

    private List<AtlasDetail> toDetailDB(List<AtlasDetailDTO> dtoList) {
        List<AtlasDetail> returnList = new ArrayList<AtlasDetail>();
        if(dtoList != null) {
            for(AtlasDetailDTO dtoEntity : dtoList) {
                AtlasDetail dbTemp = new AtlasDetail();
                BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
                returnList.add(dbTemp);
            }
        }
        return returnList;
    }
}