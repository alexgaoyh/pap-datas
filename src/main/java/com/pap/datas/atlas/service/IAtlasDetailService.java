package com.pap.datas.atlas.service;

import com.pap.datas.atlas.entity.AtlasDetail;

import java.util.List;
import java.util.Map;

public interface IAtlasDetailService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<AtlasDetail> selectListByMap(Map<Object, Object> map);

    int insert(AtlasDetail record);

    int insertSelective(AtlasDetail record);

    AtlasDetail selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(AtlasDetail record);

    int updateByPrimaryKey(AtlasDetail record);

    // alexgaoyh added begin

    List<AtlasDetail> selectListByAtlasId(String atlasId);
}
