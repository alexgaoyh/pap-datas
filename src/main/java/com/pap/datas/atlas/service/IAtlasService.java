package com.pap.datas.atlas.service;

import com.pap.datas.atlas.entity.Atlas;
import com.pap.datas.dto.AtlasDTO;

import java.util.List;
import java.util.Map;

public interface IAtlasService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<Atlas> selectListByMap(Map<Object, Object> map);

    int insert(Atlas record);

    int insertSelective(Atlas record);

    Atlas selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Atlas record);

    int updateByPrimaryKey(Atlas record);


    /**
     * 保存图集明细相关数据
     * @param operObj
     * @return
     */
    String saveAtlasWithDetailInfo(AtlasDTO operObj);

    /**
     * 更新图集明细相关数据
     * @param operObj
     * @return
     */
    String updateAtlasWithDetailInfo(AtlasDTO operObj);

    /**
     * 删除图集明细相关数据
     * @param operObj
     * @return
     */
    int deleteAtlasWithDetailInfo(AtlasDTO operObj);

    /**
     * 查询图集明细相关数据
     * @param inputSelectAtlasId
     * @return
     */
    AtlasDTO selectAtlasWithDetailInfoById(String inputSelectAtlasId, String clientLicenseId);

    /**
     * 查询图集明细相关数据
     * @param inputSelectAtlasCode
     * @return
     */
    AtlasDTO selectAtlasWithDetailInfoByCode(String inputSelectAtlasCode, String clientLicenseId);

}
