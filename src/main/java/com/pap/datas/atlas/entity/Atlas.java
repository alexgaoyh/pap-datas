package com.pap.datas.atlas.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_data_atlas", namespace = "AtlasMapper", remarks = " 修改点 ", aliasName = "t_data_atlas t_data_atlas" )
public class Atlas extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_data_atlas.ATLAS_ID
     */
    @MyBatisColumnAnnotation(name = "ATLAS_ID", value = "t_data_atlas_ATLAS_ID", chineseNote = "编号", tableAlias = "t_data_atlas")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String atlasId;

    /**
     *  编码,所属表字段为t_data_atlas.ATLAS_CODE
     */
    @MyBatisColumnAnnotation(name = "ATLAS_CODE", value = "t_data_atlas_ATLAS_CODE", chineseNote = "编码", tableAlias = "t_data_atlas")
    @MyApiModelPropertyAnnotation(value = "编码")
    private String atlasCode;

    /**
     *  名称,所属表字段为t_data_atlas.ATLAS_NAME
     */
    @MyBatisColumnAnnotation(name = "ATLAS_NAME", value = "t_data_atlas_ATLAS_NAME", chineseNote = "名称", tableAlias = "t_data_atlas")
    @MyApiModelPropertyAnnotation(value = "名称")
    private String atlasName;

    /**
     *  备注,所属表字段为t_data_atlas.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_data_atlas_REMARK", chineseNote = "备注", tableAlias = "t_data_atlas")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    /**
     *  排序号,所属表字段为t_data_atlas.ORDER_NO
     */
    @MyBatisColumnAnnotation(name = "ORDER_NO", value = "t_data_atlas_ORDER_NO", chineseNote = "排序号", tableAlias = "t_data_atlas")
    @MyApiModelPropertyAnnotation(value = "排序号")
    private String orderNo;

    /**
     *  类型,所属表字段为t_data_atlas.ATLAS_TYPE
     */
    @MyBatisColumnAnnotation(name = "ATLAS_TYPE", value = "t_data_atlas_ATLAS_TYPE", chineseNote = "类型", tableAlias = "t_data_atlas")
    @MyApiModelPropertyAnnotation(value = "类型")
    private String atlasType;

    @Override
    public String getDynamicTableName() {
        return "t_data_atlas";
    }

    private static final long serialVersionUID = 1L;

    public String getAtlasId() {
        return atlasId;
    }

    public void setAtlasId(String atlasId) {
        this.atlasId = atlasId;
    }

    public String getAtlasCode() {
        return atlasCode;
    }

    public void setAtlasCode(String atlasCode) {
        this.atlasCode = atlasCode;
    }

    public String getAtlasName() {
        return atlasName;
    }

    public void setAtlasName(String atlasName) {
        this.atlasName = atlasName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getAtlasType() {
        return atlasType;
    }

    public void setAtlasType(String atlasType) {
        this.atlasType = atlasType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", atlasId=").append(atlasId);
        sb.append(", atlasCode=").append(atlasCode);
        sb.append(", atlasName=").append(atlasName);
        sb.append(", remark=").append(remark);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", atlasType=").append(atlasType);
        sb.append("]");
        return sb.toString();
    }
}