package com.pap.datas.atlas.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.datas.atlas.entity.Atlas;
import com.pap.datas.atlas.entity.AtlasDetail;
import com.pap.datas.atlas.service.IAtlasDetailService;
import com.pap.datas.atlas.service.IAtlasService;
import com.pap.datas.dto.AtlasDTO;
import com.pap.datas.dto.AtlasDetailDTO;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/atlas")
@Api(value = "图集管理", tags = "图集管理", description="图集管理")
public class AtlasController {

	private static Logger logger  =  LoggerFactory.getLogger(AtlasController.class);
	
	@Resource(name = "atlasService")
	private IAtlasService atlasService;

	@Resource(name = "atlasDetailService")
	private IAtlasDetailService atlasDetailService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "atlasDTO", value = "", required = true, dataType = "AtlasDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<AtlasDTO> atlasCreate(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody AtlasDTO atlasDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		BeanCopyUtilss.myCopyPropertiesForLoginedUser(atlasDTO, loginedUserVO, true);
		String operInt = atlasService.saveAtlasWithDetailInfo(atlasDTO);

		AtlasDTO operAtlasDTO = atlasService.selectAtlasWithDetailInfoById(operInt, loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");

		return ResponseVO.successdata(operAtlasDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "atlasDTO", value = "", required = true, dataType = "AtlasDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<AtlasDTO> atlasUpdate(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody AtlasDTO atlasDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		BeanCopyUtilss.myCopyPropertiesForLoginedUser(atlasDTO, loginedUserVO, false);
		String operInt = atlasService.updateAtlasWithDetailInfo(atlasDTO);

		AtlasDTO operDTO = atlasService.selectAtlasWithDetailInfoById(operInt, loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");
		return ResponseVO.successdata(operDTO);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "atlasDTO", value = "", required = true, dataType = "AtlasDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> atlasDelete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody AtlasDTO atlasDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		int operInt = atlasService.deleteAtlasWithDetailInfo(atlasDTO);

		return ResponseVO.successdata(operInt);
	}

	@ApiOperation(value="根据编号查询图集信息", notes="根据编号查询图集信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "atlasId", value = "图集信息" , required = true, paramType = "path", dataType = "String")
	})
	@GetMapping(value = "/selectinfobyId/{atlasId}")
	public ResponseVO<AtlasDTO> atlasSelectInfoById(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											  @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
											  @PathVariable("atlasId") String atlasId,
											  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		AtlasDTO operAtlasDTO = atlasService.selectAtlasWithDetailInfoById(atlasId, loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");

		return ResponseVO.successdata(operAtlasDTO);
	}

	@ApiOperation(value="根据编号查询图集信息", notes="根据编号查询图集信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "atlasCode", value = "图集信息" , required = true, paramType = "path", dataType = "String")
	})
	@GetMapping(value = "/selectinfobycode/{atlasCode}")
	public ResponseVO<AtlasDTO> atlasSelectInfoByCode(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
												@PathVariable("atlasCode") String atlasCode,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		AtlasDTO operAtlasDTO = atlasService.selectAtlasWithDetailInfoByCode(atlasCode, loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");

		return ResponseVO.successdata(operAtlasDTO);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "atlasDTO", value = "应用查询参数", required = false, dataType = "AtlasDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<AtlasDTO> atlasQuery(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody AtlasDTO atlasDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(atlasDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<Atlas> list = atlasService.selectListByMap(map);

		//
		List<AtlasDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = atlasService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	@ApiOperation("查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "atlasDTO", value = "应用查询参数", required = false, dataType = "AtlasDTO", paramType="body"),
			@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
			@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
			@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/querywithdetail")
	public ResponseVO<AtlasDTO> atlasQueryWithDetail(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
									 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
									 @RequestBody AtlasDTO atlasDTO,
									 @RequestParam Integer pageNo, @RequestParam Integer pageSize,
									 @RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
									 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(atlasDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<Atlas> list = atlasService.selectListByMap(map);

		//
		List<AtlasDTO> dtoList = toDTOWithDetail(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = atlasService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<AtlasDTO> toDTO(List<Atlas> databaseList) {
		List<AtlasDTO> returnList = new ArrayList<AtlasDTO>();
		if(databaseList != null) {
			for(Atlas dbEntity : databaseList) {
				AtlasDTO dtoTemp = new AtlasDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<AtlasDTO> toDTOWithDetail(List<Atlas> databaseList) {
		List<AtlasDTO> returnList = new ArrayList<AtlasDTO>();
		if(databaseList != null) {
			for(Atlas dbEntity : databaseList) {
				AtlasDTO dtoTemp = new AtlasDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);

				List<AtlasDetail> atlasDetails = atlasDetailService.selectListByAtlasId(dtoTemp.getAtlasId());
				dtoTemp.setDetails(toDTOForDetail(atlasDetails));
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<AtlasDetailDTO> toDTOForDetail(List<AtlasDetail> databaseList) {
		List<AtlasDetailDTO> returnList = new ArrayList<AtlasDetailDTO>();
		if(databaseList != null) {
			for(AtlasDetail dbEntity : databaseList) {
				AtlasDetailDTO dtoTemp = new AtlasDetailDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<Atlas> toDB(List<AtlasDTO> dtoList) {
		List<Atlas> returnList = new ArrayList<Atlas>();
		if(dtoList != null) {
			for(AtlasDTO dtoEntity : dtoList) {
				Atlas dbTemp = new Atlas();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
