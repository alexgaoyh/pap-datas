package com.pap.datas.atlas.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.datas.atlas.entity.AtlasDetail;
import com.pap.datas.atlas.service.IAtlasDetailService;
import com.pap.datas.dto.AtlasDetailDTO;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


//@RestController
//@RequestMapping("/atlasdetail")
//@Api(value = "", tags = "", description="")
@Deprecated
public class AtlasDetailController {

	private static Logger logger  =  LoggerFactory.getLogger(AtlasDetailController.class);
	
	@Resource(name = "atlasDetailService")
	private IAtlasDetailService atlasDetailService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "atlasDetailDTO", value = "", required = true, dataType = "AtlasDetailDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<AtlasDetailDTO> atlasDetailCreate(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											 @RequestBody AtlasDetailDTO atlasDetailDTO,
											 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		// TODO 设置主键
		atlasDetailDTO.setAtlasDetailId(idStr);

		AtlasDetail databaseObj = new AtlasDetail();
		BeanCopyUtilss.myCopyProperties(atlasDetailDTO, databaseObj, loginedUserVO);

		int i = atlasDetailService.insertSelective(databaseObj);

		return ResponseVO.successdata(atlasDetailDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "atlasDetailDTO", value = "", required = true, dataType = "AtlasDetailDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<AtlasDetailDTO> atlasDetailUpdate(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody AtlasDetailDTO atlasDetailDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		AtlasDetail databaseObj = new AtlasDetail();
		BeanCopyUtilss.myCopyProperties(atlasDetailDTO, databaseObj, loginedUserVO, false);
		int i = atlasDetailService.updateByPrimaryKeySelective(databaseObj);

		// TODO 主键部分
		databaseObj = atlasDetailService.selectByPrimaryKey(databaseObj.getAtlasDetailId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "atlasDetailDTO", value = "", required = true, dataType = "AtlasDetailDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> atlasDetailDelete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody AtlasDetailDTO atlasDetailDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		AtlasDetail databaseObj = new AtlasDetail();
		BeanCopyUtilss.myCopyProperties(atlasDetailDTO, databaseObj);

		// TODO 主键部分
		int i = atlasDetailService.deleteByPrimaryKey(databaseObj.getAtlasDetailId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<AtlasDetailDTO> atlasDetailSelectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		AtlasDetail databaseObj = atlasDetailService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "atlasDetailDTO", value = "应用查询参数", required = false, dataType = "AtlasDetailDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<AtlasDetailDTO> atlasDetailQuery(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody AtlasDetailDTO atlasDetailDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(atlasDetailDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<AtlasDetail> list = atlasDetailService.selectListByMap(map);

		//
		List<AtlasDetailDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = atlasDetailService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<AtlasDetailDTO> toDTO(List<AtlasDetail> databaseList) {
		List<AtlasDetailDTO> returnList = new ArrayList<AtlasDetailDTO>();
		if(databaseList != null) {
			for(AtlasDetail dbEntity : databaseList) {
				AtlasDetailDTO dtoTemp = new AtlasDetailDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<AtlasDetail> toDB(List<AtlasDetailDTO> dtoList) {
		List<AtlasDetail> returnList = new ArrayList<AtlasDetail>();
		if(dtoList != null) {
			for(AtlasDetailDTO dtoEntity : dtoList) {
				AtlasDetail dbTemp = new AtlasDetail();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
