package com.pap.datas.area.controller;

import com.pap.datas.area.service.IBaseAreaService;
import com.pap.datas.dto.BaseAreaLeafDTO;
import com.pap.datas.dto.BaseAreaTreeNodeVO;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/basearea")
@Api(value = "区域管理", tags = "区域管理", description="区域管理")
public class BaseAreaController {

	private static Logger logger  =  LoggerFactory.getLogger(BaseAreaController.class);

	@Resource(name = "baseAreaService")
	private IBaseAreaService baseAreaService;

	@ApiOperation(value="获取区域树形结构", notes="获取区域树形结构")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
	})
	@RequestMapping(value = "/treejson", method = {RequestMethod.GET})
	public ResponseVO<List<BaseAreaTreeNodeVO>> baseAreaTreeJSON(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											 @RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion) {
		//  全局默认组织机构的顶层编码为 -1
		return ResponseVO.successdatas(baseAreaService.baseAreaTreeJson(), null);
	}

	@ApiOperation(value="根据父级编号获取下一级区域信息", notes="根据父级编号获取下一级区域信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "parentId", value = "父级区域编号", required = true, paramType = "path", dataType = "String")
	})
	@RequestMapping(value = "/selectchildrenbyid/{parentId}", method = {RequestMethod.GET})
	public ResponseVO<List<BaseAreaLeafDTO>> baseAreaSelectChildrenById(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "-1") String papVersion,
													@PathVariable String parentId) {
		return ResponseVO.successdatas(baseAreaService.baseAreaLeafJson(parentId),null);
	}

}
