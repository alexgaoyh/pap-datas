package com.pap.datas.area.service.impl;

import com.pap.base.util.string.StringUtilss;
import com.pap.datas.area.entity.BaseArea;
import com.pap.datas.area.mapper.BaseAreaMapper;
import com.pap.datas.area.service.IBaseAreaService;
import com.pap.datas.dto.BaseAreaLeafDTO;
import com.pap.datas.dto.BaseAreaTreeNodeVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("baseAreaService")
public class BaseAreaServiceImpl implements IBaseAreaService {

    @Resource(name = "baseAreaMapper")
    private BaseAreaMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<BaseArea> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(BaseArea record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(BaseArea record) {
       return mapper.insertSelective(record);
    }

    @Override
    public BaseArea selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(BaseArea record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(BaseArea record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public List<BaseAreaTreeNodeVO> baseAreaTreeJson() {
        return mapper.selectBaseAreaTreeJson();
    }

    @Override
    public List<BaseAreaLeafDTO> baseAreaLeafJson(String parentId) {
        return mapper.selectBaseAreaLeafJson(parentId);
    }

    @Override
    public String selectAreaNamesByAreaCodes(String areaCodes) {
        if(StringUtilss.isNotEmpty(areaCodes)) {
            String[] areaCodeArray = areaCodes.split(",");
            String[] areaNameArray = new String[areaCodeArray.length];
            for(int i = 0; i < areaCodeArray.length; i++) {
                BaseArea baseArea = mapper.selectByPrimaryKey(areaCodeArray[i]);
                if(baseArea != null && StringUtilss.isNotEmpty(baseArea.getAreaId())) {
                    areaNameArray[i] = baseArea.getAreaName();
                }
            }
            return StringUtils.join(areaNameArray,",");
        }
        return "";
    }
}