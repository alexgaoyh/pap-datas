package com.pap.datas.area.service;

import com.pap.datas.area.entity.BaseArea;
import com.pap.datas.dto.BaseAreaLeafDTO;
import com.pap.datas.dto.BaseAreaTreeNodeVO;

import java.util.List;
import java.util.Map;

public interface IBaseAreaService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<BaseArea> selectListByMap(Map<Object, Object> map);

    int insert(BaseArea record);

    int insertSelective(BaseArea record);

    BaseArea selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BaseArea record);

    int updateByPrimaryKey(BaseArea record);

    /**
     * 省市区树形结构
     */
    List<BaseAreaTreeNodeVO> baseAreaTreeJson();

    List<BaseAreaLeafDTO> baseAreaLeafJson(String parentId);

    /**
     * 根据逗号分隔的省市区编号集合，返回逗号分隔的省市区名称集合
     * @param areaCodes
     * @return
     */
    String selectAreaNamesByAreaCodes(String areaCodes);
}
