package com.pap.datas.area.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_data_area", namespace = "com.pap.datas.area.mapper.BaseAreaMapper", remarks = " 修改点 ", aliasName = "t_data_area t_data_area" )
public class BaseArea extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_data_area.AREA_ID
     */
    @MyBatisColumnAnnotation(name = "AREA_ID", value = "t_data_area_AREA_ID", chineseNote = "编号", tableAlias = "t_data_area")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String areaId;

    /**
     *  名称,所属表字段为t_data_area.AREA_NAME
     */
    @MyBatisColumnAnnotation(name = "AREA_NAME", value = "t_data_area_AREA_NAME", chineseNote = "名称", tableAlias = "t_data_area")
    @MyApiModelPropertyAnnotation(value = "名称")
    private String areaName;

    /**
     *  父级编号,所属表字段为t_data_area.AREA_PARENT_ID
     */
    @MyBatisColumnAnnotation(name = "AREA_PARENT_ID", value = "t_data_area_AREA_PARENT_ID", chineseNote = "父级编号", tableAlias = "t_data_area")
    @MyApiModelPropertyAnnotation(value = "父级编号")
    private String areaParentId;

    /**
     *  级别,所属表字段为t_data_area.LEVEL
     */
    @MyBatisColumnAnnotation(name = "LEVEL", value = "t_data_area_LEVEL", chineseNote = "级别", tableAlias = "t_data_area")
    @MyApiModelPropertyAnnotation(value = "级别")
    private String level;

    private static final long serialVersionUID = 1L;

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaParentId() {
        return areaParentId;
    }

    public void setAreaParentId(String areaParentId) {
        this.areaParentId = areaParentId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", areaId=").append(areaId);
        sb.append(", areaName=").append(areaName);
        sb.append(", areaParentId=").append(areaParentId);
        sb.append(", level=").append(level);
        sb.append("]");
        return sb.toString();
    }
}