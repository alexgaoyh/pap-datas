package com.pap.datas.area.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.datas.area.entity.BaseArea;
import com.pap.datas.dto.BaseAreaLeafDTO;
import com.pap.datas.dto.BaseAreaTreeNodeVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BaseAreaMapper extends PapBaseMapper<BaseArea> {
    int deleteByPrimaryKey(String areaId);

    int selectCountByMap(Map<Object, Object> map);

    List<BaseArea> selectListByMap(Map<Object, Object> map);

    BaseArea selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(BaseArea record);

    int insertSelective(BaseArea record);

    BaseArea selectByPrimaryKey(String areaId);

    int updateByPrimaryKeySelective(BaseArea record);

    int updateByPrimaryKey(BaseArea record);


    /**
     * 省市区树形结构
     */
    List<BaseAreaTreeNodeVO> selectBaseAreaTreeJson();

    List<BaseAreaLeafDTO> selectBaseAreaLeafJson(@Param("parentId") String parentId);

}