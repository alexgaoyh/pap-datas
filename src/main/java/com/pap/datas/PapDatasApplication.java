package com.pap.datas;

import com.pap.beans.annotation.LoginedUserArgumentResolver;
import org.apache.ibatis.mapping.DatabaseIdProvider;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;
import java.util.Properties;

@SpringBootApplication
@EnableTransactionManagement
@EnableSwagger2
@MapperScan("com.pap.datas")
// @EnableEurekaClient
public class PapDatasApplication  implements WebMvcConfigurer {

	@Autowired
	private LoginedUserArgumentResolver loginedUserArgumentResolver;

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(loginedUserArgumentResolver);
	}
//
//	@Autowired
//	private RequestMappingHandlerAdapter requestMappingHandlerAdapter;
//
//	@Bean
//	public HandlerMethodReturnValueHandler completableFutureReturnValueHandler() {
//		return new PapJacksonFilterReturnHandler();
//	}
//
//	@PostConstruct
//	public void init() {
//		final List<HandlerMethodReturnValueHandler> originalHandlers = new ArrayList<>(
//				requestMappingHandlerAdapter.getReturnValueHandlers());
//
//		final int deferredPos = obtainValueHandlerPosition(originalHandlers, DeferredResultMethodReturnValueHandler.class);
//		// Add our handler directly after the deferred handler.
//		originalHandlers.add(deferredPos + 1, completableFutureReturnValueHandler());
//
//		requestMappingHandlerAdapter.setReturnValueHandlers(originalHandlers);
//	}
//
//	private int obtainValueHandlerPosition(final List<HandlerMethodReturnValueHandler> originalHandlers, Class<?> handlerClass) {
//		for (int i = 0; i < originalHandlers.size(); i++) {
//			final HandlerMethodReturnValueHandler valueHandler = originalHandlers.get(i);
//			if (handlerClass.isAssignableFrom(valueHandler.getClass())) {
//				return i;
//			}
//		}
//		return -1;
//	}

	private CorsConfiguration buildConfig() {
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		// 允许任何域名使用
		corsConfiguration.addAllowedOrigin("*");
		// 允许任何头
		corsConfiguration.addAllowedHeader("*");
		// 允许任何方法（post、get等）
		corsConfiguration.addAllowedMethod("*");
		return corsConfiguration;
	}

	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", buildConfig());
		return new CorsFilter(source);
	}

	/**
	 *	swagger.ui.show.flag 没有设置值，swaggerUiShowFlag 变量值将会被设置成 true 。
	 */
	@Value("${swagger.ui.show.flag:true}")
	private Boolean swaggerUiShowFlag;

	@Bean
	public Docket buildDocket() {
		ApiSelectorBuilder apiSelectorBuilder = new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(buildApiInfo())
				.select();
		if(swaggerUiShowFlag) {
			return apiSelectorBuilder// 要扫描的API(Controller)基础包
				.apis(RequestHandlerSelectors
					.basePackage("com.pap.datas"))
					.paths(PathSelectors.any()).build();
		} else {
			return apiSelectorBuilder// 要扫描的API(Controller)基础包
					.apis(RequestHandlerSelectors
							.none())
					.paths(PathSelectors.any()).build();
		}

	}

	/**
	 * @param
	 * @return springfox.documentation.service.ApiInfo
	 * @Title: 构建API基本信息
	 * @methodName: buildApiInfo
	 */
	private ApiInfo buildApiInfo() {
		return new ApiInfoBuilder().title("统一用户字典&区域-资源服务API文档")
				.description("数据字典、区域服务api").contact(new Contact("alexgaoyh", "https://git.oschina.net/alexgaoyh", null))
				.version("1.0").build();

	}

	/**
	 * swagger-ui.html相关的所有前端静态文件都在springfox-swagger-ui-2.6.1.jar里面
	 * 把/swagger-ui.html这个路径映射到对应的目录META-INF/resources/下面
	 * @param registry
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("swagger-ui.html")
				.addResourceLocations("classpath:/META-INF/resources/");

		registry.addResourceHandler("/webjars/**")
				.addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	@Bean
	public DatabaseIdProvider getDatabaseIdProvider() {
		DatabaseIdProvider databaseIdProvider = new VendorDatabaseIdProvider();
		Properties p = new Properties();
		p.setProperty("Oracle", "oracle");
		p.setProperty("MySQL", "mysql");
		databaseIdProvider.setProperties(p);
		return databaseIdProvider;
	}



	public static void main(String[] args) {
		// Hibernate 默认使用日志jar为 org.jboss.logging,需要设置环境变量为 slf4j, 避免日志不能正确输出.
		System.setProperty("org.jboss.logging.provider", "slf4j");
		SpringApplication.run(PapDatasApplication.class, args);
	}
}
