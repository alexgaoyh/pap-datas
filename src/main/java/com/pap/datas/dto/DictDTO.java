package com.pap.datas.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.validation.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@ApiModel(description = "字典操作实体类")
public class DictDTO extends PapBaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull(message = "编号不能为空", groups = {UpdateGroup.class})
	@NotEmpty(message = "编号不能为空", groups = {UpdateGroup.class})
	@ApiModelProperty(notes = "编号")
	private String dictId;
	
	/**
	 * 编码,所属表字段为t_data_dict.CODE
	 */
	@NotNull(message = "编码不能为空")
	@NotEmpty(message = "编码不能为空")
	@ApiModelProperty(notes = "编码")
	private String dictCode;

	/**
	 * 名称,所属表字段为t_data_dict.NAME
	 */
	@NotNull(message = "名称不能为空")
	@NotEmpty(message = "名称不能为空")
	@ApiModelProperty(notes = "名称")
	private String dictName;

	/**
	 * 备注,所属表字段为t_data_dict.REMARK
	 */
	@ApiModelProperty(notes = "备注")
	private String remark;

	/**
	 * 排序号,所属表字段为t_data_dict.ORDER_NO
	 */
	@ApiModelProperty(notes = "排序号")
	private String orderNo;

	/**
	 * 字典类型,所属表字段为t_data_dict.DICT_TYPE
	 */
	@ApiModelProperty(notes = "字典类型")
	private String dictType;

	@ApiModelProperty(notes = "字典明细")
	private List<DictDetailDTO> details;

	@Override
	public String getDynamicTableName() {
		return "t_data_dict";
	}

	public String getDictId() {
		return dictId;
	}

	public void setDictId(String dictId) {
		this.dictId = dictId;
	}

	public String getDictCode() {
		return dictCode;
	}

	public void setDictCode(String dictCode) {
		this.dictCode = dictCode;
	}

	public String getDictName() {
		return dictName;
	}

	public void setDictName(String dictName) {
		this.dictName = dictName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getDictType() {
		return dictType;
	}

	public void setDictType(String dictType) {
		this.dictType = dictType;
	}

	public List<DictDetailDTO> getDetails() {
		return details;
	}

	public void setDetails(List<DictDetailDTO> details) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "DictDTO{" +
				"dictId='" + dictId + '\'' +
				", dictCode='" + dictCode + '\'' +
				", dictName='" + dictName + '\'' +
				", remark='" + remark + '\'' +
				", orderNo='" + orderNo + '\'' +
				", dictType='" + dictType + '\'' +
				", details=" + details +
				'}';
	}
}
