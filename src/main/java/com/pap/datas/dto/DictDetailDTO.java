package com.pap.datas.dto;

import com.pap.base.entity.PapBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "字典操作明细类")
public class DictDetailDTO extends PapBaseEntity implements Serializable  {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 编号
	 */
	@ApiModelProperty(notes = "编号")
	private String dictDetailId;

	/**
     *  编码,所属表字段为t_data_dict_detail.CODE
     */
	@ApiModelProperty(notes = "编码")
    private String dictDetailCode;

    /**
     *  名称,所属表字段为t_data_dict_detail.NAME
     */
	@ApiModelProperty(notes = "名称")
    private String dictDetailName;

	/**
	 *  所属字典编号,所属表字段为t_data_dict_detail.DICT_ID
	 */
	@ApiModelProperty(value = "所属字典编号")
	private String dictId;

	/**
	 * 数据值,所属表字段为t_data_dict_detail.NUM_VALUE
	 */
	@ApiModelProperty(notes = "数据值")
	private String numValue;

	/**
	 * 字符值,所属表字段为t_data_dict_detail.STR_VALUE
	 */
	@ApiModelProperty(notes = "字符值")
	private String strValue;

	@Override
	public String getDynamicTableName() {
		return "t_data_dict_detail";
	}

	public String getDictDetailId() {
		return dictDetailId;
	}

	public void setDictDetailId(String dictDetailId) {
		this.dictDetailId = dictDetailId;
	}

	public String getDictDetailCode() {
		return dictDetailCode;
	}

	public void setDictDetailCode(String dictDetailCode) {
		this.dictDetailCode = dictDetailCode;
	}

	public String getDictDetailName() {
		return dictDetailName;
	}

	public void setDictDetailName(String dictDetailName) {
		this.dictDetailName = dictDetailName;
	}

	public String getDictId() {
		return dictId;
	}

	public void setDictId(String dictId) {
		this.dictId = dictId;
	}

	public String getNumValue() {
		return numValue;
	}

	public void setNumValue(String numValue) {
		this.numValue = numValue;
	}

	public String getStrValue() {
		return strValue;
	}

	public void setStrValue(String strValue) {
		this.strValue = strValue;
	}

	@Override
	public String toString() {
		return "DictDetailDTO{" +
				"dictDetailId='" + dictDetailId + '\'' +
				", dictDetailCode='" + dictDetailCode + '\'' +
				", dictDetailName='" + dictDetailName + '\'' +
				", dictId='" + dictId + '\'' +
				", numValue='" + numValue + '\'' +
				", strValue='" + strValue + '\'' +
				'}';
	}
}
