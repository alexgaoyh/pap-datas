package com.pap.datas.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "图集明细类")
public class AtlasDetailDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_data_atlas_detail.ATLAS__DETAIL_ID
     */
    @MyBatisColumnAnnotation(name = "ATLAS__DETAIL_ID", value = "t_data_atlas_detail_ATLAS__DETAIL_ID", chineseNote = "编号", tableAlias = "t_data_atlas_detail")
    @ApiModelProperty(value = "编号")
    private String atlasDetailId;

    /**
     *  编码,所属表字段为t_data_atlas_detail.ATLAS__DETAIL_CODE
     */
    @MyBatisColumnAnnotation(name = "ATLAS__DETAIL_CODE", value = "t_data_atlas_detail_ATLAS__DETAIL_CODE", chineseNote = "编码", tableAlias = "t_data_atlas_detail")
    @ApiModelProperty(value = "编码")
    private String atlasDetailCode;

    /**
     *  名称,所属表字段为t_data_atlas_detail.ATLAS__DETAIL_NAME
     */
    @MyBatisColumnAnnotation(name = "ATLAS__DETAIL_NAME", value = "t_data_atlas_detail_ATLAS__DETAIL_NAME", chineseNote = "名称", tableAlias = "t_data_atlas_detail")
    @ApiModelProperty(value = "名称")
    private String atlasDetailName;

    /**
     *  所属图集编号,所属表字段为t_data_atlas_detail.ATLAS_ID
     */
    @MyBatisColumnAnnotation(name = "ATLAS_ID", value = "t_data_atlas_detail_ATLAS_ID", chineseNote = "所属图集编号", tableAlias = "t_data_atlas_detail")
    @ApiModelProperty(value = "所属图集编号")
    private String atlasId;

    /**
     *  文件名称,所属表字段为t_data_atlas_detail.FILE_NAME
     */
    @MyBatisColumnAnnotation(name = "FILE_NAME", value = "t_data_atlas_detail_FILE_NAME", chineseNote = "文件名称", tableAlias = "t_data_atlas_detail")
    @ApiModelProperty(value = "文件名称")
    private String fileName;

    /**
     *  文件路径,所属表字段为t_data_atlas_detail.FILE_URL
     */
    @MyBatisColumnAnnotation(name = "FILE_URL", value = "t_data_atlas_detail_FILE_URL", chineseNote = "文件路径", tableAlias = "t_data_atlas_detail")
    @ApiModelProperty(value = "文件路径")
    private String fileUrl;

    /**
     *  文件备注,所属表字段为t_data_atlas_detail.FILE_REMARK
     */
    @MyBatisColumnAnnotation(name = "FILE_REMARK", value = "t_data_atlas_detail_FILE_REMARK", chineseNote = "文件备注", tableAlias = "t_data_atlas_detail")
    @ApiModelProperty(value = "文件备注")
    private String fileRemark;

    @Override
    public String getDynamicTableName() {
        return "t_data_atlas_detail";
    }

    private static final long serialVersionUID = 1L;

    public String getAtlasDetailId() {
        return atlasDetailId;
    }

    public void setAtlasDetailId(String atlasDetailId) {
        this.atlasDetailId = atlasDetailId;
    }

    public String getAtlasDetailCode() {
        return atlasDetailCode;
    }

    public void setAtlasDetailCode(String atlasDetailCode) {
        this.atlasDetailCode = atlasDetailCode;
    }

    public String getAtlasDetailName() {
        return atlasDetailName;
    }

    public void setAtlasDetailName(String atlasDetailName) {
        this.atlasDetailName = atlasDetailName;
    }

    public String getAtlasId() {
        return atlasId;
    }

    public void setAtlasId(String atlasId) {
        this.atlasId = atlasId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileRemark() {
        return fileRemark;
    }

    public void setFileRemark(String fileRemark) {
        this.fileRemark = fileRemark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", atlasDetailId=").append(atlasDetailId);
        sb.append(", atlasDetailCode=").append(atlasDetailCode);
        sb.append(", atlasDetailName=").append(atlasDetailName);
        sb.append(", atlasId=").append(atlasId);
        sb.append(", fileName=").append(fileName);
        sb.append(", fileUrl=").append(fileUrl);
        sb.append(", fileRemark=").append(fileRemark);
        sb.append("]");
        return sb.toString();
    }
}