package com.pap.datas.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pap.datas.area.entity.BaseArea;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseAreaTreeNodeVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String name;
	
	private String parentId;
	
	private List<BaseArea> children = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public List<BaseArea> getChildren() {
		if(children == null || children.size() == 0) {
			return null;
		} else {
			return children;
		}
	}

	public void setChildren(List<BaseArea> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "BaseAreaTreeNodeVO [id=" + id + ", name=" + name + ", parentId=" + parentId + ", children=" + children
				+ "]";
	}
	
}
