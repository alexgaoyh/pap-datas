package com.pap.datas.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import com.pap.base.mybatis.plugin.annotation.MyBatisTableAnnotation;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_data_properties", namespace = "com.pap.datas.properties.mapper.DataPropertiesMapper", remarks = " 修改点 ", aliasName = "t_data_properties t_data_properties" )
public class DataPropertiesDTO extends PapBaseEntity implements Serializable {
    /**
     *  数据配置参数编号,所属表字段为t_data_properties.DATA_PROPERTIES_ID
     */
    @MyBatisColumnAnnotation(name = "DATA_PROPERTIES_ID", value = "t_data_properties_DATA_PROPERTIES_ID", chineseNote = "数据配置参数编号", tableAlias = "t_data_properties", methodName = "dataPropertiesId")
    @ApiModelProperty(value = "数据配置参数编号")
    private String dataPropertiesId;

    /**
     *  数据配置参数编码,所属表字段为t_data_properties.DATA_PROPERTIES_CODE
     */
    @MyBatisColumnAnnotation(name = "DATA_PROPERTIES_CODE", value = "t_data_properties_DATA_PROPERTIES_CODE", chineseNote = "数据配置参数编码", tableAlias = "t_data_properties", methodName = "dataPropertiesCode")
    @ApiModelProperty(value = "数据配置参数编码")
    private String dataPropertiesCode;

    /**
     *  数据配置参数类型,所属表字段为t_data_properties.DATA_PROPERTIES_TYPE
     */
    @MyBatisColumnAnnotation(name = "DATA_PROPERTIES_TYPE", value = "t_data_properties_DATA_PROPERTIES_TYPE", chineseNote = "数据配置参数类型", tableAlias = "t_data_properties", methodName = "dataPropertiesType")
    @ApiModelProperty(value = "数据配置参数类型")
    private String dataPropertiesType;

    /**
     *  数据配置参数操作名称,所属表字段为t_data_properties.DATA_PROPERTIES_NAME
     */
    @MyBatisColumnAnnotation(name = "DATA_PROPERTIES_NAME", value = "t_data_properties_DATA_PROPERTIES_NAME", chineseNote = "数据配置参数操作名称", tableAlias = "t_data_properties", methodName = "dataPropertiesName")
    @ApiModelProperty(value = "数据配置参数操作名称")
    private String dataPropertiesName;

    /**
     *  数据配置参数操作符,所属表字段为t_data_properties.DATA_PROPERTIES_OPERATION
     */
    @MyBatisColumnAnnotation(name = "DATA_PROPERTIES_OPERATION", value = "t_data_properties_DATA_PROPERTIES_OPERATION", chineseNote = "数据配置参数操作符", tableAlias = "t_data_properties", methodName = "dataPropertiesOperation")
    @ApiModelProperty(value = "数据配置参数操作符")
    private String dataPropertiesOperation;

    /**
     *  数据配置参数操作值,所属表字段为t_data_properties.DATA_PROPERTIES_VALUE
     */
    @MyBatisColumnAnnotation(name = "DATA_PROPERTIES_VALUE", value = "t_data_properties_DATA_PROPERTIES_VALUE", chineseNote = "数据配置参数操作值", tableAlias = "t_data_properties", methodName = "dataPropertiesValue")
    @ApiModelProperty(value = "数据配置参数操作值")
    private String dataPropertiesValue;

    @Override
    public String getDynamicTableName() {
        return "t_data_properties";
    }

    private static final long serialVersionUID = 1L;

    public String getDataPropertiesId() {
        return dataPropertiesId;
    }

    public void setDataPropertiesId(String dataPropertiesId) {
        this.dataPropertiesId = dataPropertiesId;
    }

    public String getDataPropertiesCode() {
        return dataPropertiesCode;
    }

    public void setDataPropertiesCode(String dataPropertiesCode) {
        this.dataPropertiesCode = dataPropertiesCode;
    }

    public String getDataPropertiesType() {
        return dataPropertiesType;
    }

    public void setDataPropertiesType(String dataPropertiesType) {
        this.dataPropertiesType = dataPropertiesType;
    }

    public String getDataPropertiesName() {
        return dataPropertiesName;
    }

    public void setDataPropertiesName(String dataPropertiesName) {
        this.dataPropertiesName = dataPropertiesName;
    }

    public String getDataPropertiesOperation() {
        return dataPropertiesOperation;
    }

    public void setDataPropertiesOperation(String dataPropertiesOperation) {
        this.dataPropertiesOperation = dataPropertiesOperation;
    }

    public String getDataPropertiesValue() {
        return dataPropertiesValue;
    }

    public void setDataPropertiesValue(String dataPropertiesValue) {
        this.dataPropertiesValue = dataPropertiesValue;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dataPropertiesId=").append(dataPropertiesId);
        sb.append(", dataPropertiesCode=").append(dataPropertiesCode);
        sb.append(", dataPropertiesType=").append(dataPropertiesType);
        sb.append(", dataPropertiesName=").append(dataPropertiesName);
        sb.append(", dataPropertiesOperation=").append(dataPropertiesOperation);
        sb.append(", dataPropertiesValue=").append(dataPropertiesValue);
        sb.append("]");
        return sb.toString();
    }
}