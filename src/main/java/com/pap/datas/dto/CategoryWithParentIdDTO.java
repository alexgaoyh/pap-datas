package com.pap.datas.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import com.pap.base.mybatis.plugin.annotation.MyBatisTableAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "目录集操作对象")
@MyBatisTableAnnotation(name = "t_data_category", namespace = "com.pap.datas.category.mapper.CategoryMapper", remarks = " 修改点 ", aliasName = "t_data_category t_data_category" )
public class CategoryWithParentIdDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_data_category.CATEGORY_ID
     */
    @MyBatisColumnAnnotation(name = "CATEGORY_ID", value = "t_data_category_CATEGORY_ID", chineseNote = "编号", tableAlias = "t_data_category", orderNo = 1, methodName = "categoryId")
    @ApiModelProperty(value = "编号")
    private String categoryId;

    /**
     *  编码,所属表字段为t_data_category.CATEGORY_CODE
     */
    @MyBatisColumnAnnotation(name = "CATEGORY_CODE", value = "t_data_category_CATEGORY_CODE", chineseNote = "编码", tableAlias = "t_data_category", orderNo = 2, methodName = "categoryCode")
    @ApiModelProperty(value = "编码")
    private String categoryCode;

    /**
     *  名称,所属表字段为t_data_category.CATEGORY_NAME
     */
    @MyBatisColumnAnnotation(name = "CATEGORY_NAME", value = "t_data_category_CATEGORY_NAME", chineseNote = "名称", tableAlias = "t_data_category", orderNo = 3, methodName = "categoryName")
    @ApiModelProperty(value = "名称")
    private String categoryName;

    /**
     *  备注,所属表字段为t_data_category.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_data_category_REMARK", chineseNote = "备注", tableAlias = "t_data_category", orderNo = 4, methodName = "remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     *  排序号,所属表字段为t_data_category.ORDER_NO
     */
    @MyBatisColumnAnnotation(name = "ORDER_NO", value = "t_data_category_ORDER_NO", chineseNote = "排序号", tableAlias = "t_data_category", orderNo = 5, methodName = "orderNo")
    @ApiModelProperty(value = "排序号")
    private Integer orderNo;

    /**
     *  类型,所属表字段为t_data_category.CATEGORY_TYPE
     */
    @MyBatisColumnAnnotation(name = "CATEGORY_TYPE", value = "t_data_category_CATEGORY_TYPE", chineseNote = "类型", tableAlias = "t_data_category", orderNo = 6, methodName = "categoryType")
    @ApiModelProperty(value = "类型")
    private String categoryType;

    @ApiModelProperty(value = "父级编号")
    private String categoryParentId;

    private static final long serialVersionUID = 1L;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryParentId() {
        return categoryParentId;
    }

    public void setCategoryParentId(String categoryParentId) {
        this.categoryParentId = categoryParentId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"categoryId\":\"")
                .append(categoryId).append('\"');
        sb.append(",\"categoryCode\":\"")
                .append(categoryCode).append('\"');
        sb.append(",\"categoryName\":\"")
                .append(categoryName).append('\"');
        sb.append(",\"remark\":\"")
                .append(remark).append('\"');
        sb.append(",\"orderNo\":\"")
                .append(orderNo).append('\"');
        sb.append(",\"categoryType\":\"")
                .append(categoryType).append('\"');
        sb.append(",\"categoryParentId\":\"")
                .append(categoryParentId).append('\"');
        sb.append('}');
        return sb.toString();
    }
}