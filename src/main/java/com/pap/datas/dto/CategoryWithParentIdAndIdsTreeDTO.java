package com.pap.datas.dto;

import com.pap.base.util.bean.BeanCopyUtilss;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ApiModel(description = "目录集操作对象")
public class CategoryWithParentIdAndIdsTreeDTO extends CategoryWithParentIdAndIdsDTO implements Serializable {

    private List<CategoryWithParentIdAndIdsTreeDTO> children;

    public List<CategoryWithParentIdAndIdsTreeDTO> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryWithParentIdAndIdsTreeDTO> children) {
        this.children = children;
    }

    // 工具类， 处理树形结构变化

    public static List<CategoryWithParentIdAndIdsTreeDTO> revertToTree(List<CategoryWithParentIdAndIdsDTO> list, String parentId) {
        List<CategoryWithParentIdAndIdsTreeDTO> categoryWithParentIdAndIdsTreeDTOList = beanCopyReplaceParentId(list);
        return getThree(categoryWithParentIdAndIdsTreeDTOList, "-1");
    }

    private static List<CategoryWithParentIdAndIdsTreeDTO> beanCopyReplaceParentId(List<CategoryWithParentIdAndIdsDTO> categoryWithParentIdAndIdsDTOList) {
        List<CategoryWithParentIdAndIdsTreeDTO> categoryWithParentIdAndIdsTreeDTOList = new ArrayList<>();
        if(categoryWithParentIdAndIdsDTOList != null && categoryWithParentIdAndIdsDTOList.size() >= 0) {
            for(CategoryWithParentIdAndIdsDTO categoryWithParentIdAndIdsDTO : categoryWithParentIdAndIdsDTOList) {
                CategoryWithParentIdAndIdsTreeDTO temp = new CategoryWithParentIdAndIdsTreeDTO();
                BeanCopyUtilss.myCopyProperties(categoryWithParentIdAndIdsDTO, temp);
                if(temp.getCategoryParentId() == null) {
                    temp.setCategoryParentId("-1");
                }
                categoryWithParentIdAndIdsTreeDTOList.add(temp);
            }
        }
        return categoryWithParentIdAndIdsTreeDTOList;
    }

    private static List<CategoryWithParentIdAndIdsTreeDTO> getThree(List<CategoryWithParentIdAndIdsTreeDTO> list, String parentId) {
        //获取所有子节点
        List<CategoryWithParentIdAndIdsTreeDTO> childTreeList = getChildTree(list, parentId);
        if(childTreeList != null) {
            for (CategoryWithParentIdAndIdsTreeDTO dtoTemp : childTreeList) {
                dtoTemp.setChildren(getThree(list, dtoTemp.getCategoryId()));
            }
        }
        return childTreeList;
    }

    private static List<CategoryWithParentIdAndIdsTreeDTO> getChildTree(List<CategoryWithParentIdAndIdsTreeDTO> list, String id) {
        List<CategoryWithParentIdAndIdsTreeDTO> childTree = new ArrayList<>();
        for (CategoryWithParentIdAndIdsTreeDTO dept : list) {
            if (dept.getCategoryParentId().equals(id)) {
                childTree.add(dept);
            }
        }
        if(childTree != null && childTree.size() > 0) {
            return childTree;
        } else {
            return null;
        }
    }
}