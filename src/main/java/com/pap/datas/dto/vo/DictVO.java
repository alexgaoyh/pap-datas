package com.pap.datas.dto.vo;

import com.pap.base.validation.IsPapName;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 定义一个VO类，并且增加参数注解部分。
 * @Auther: alexgaoyh
 * @Date: 2018/11/28 17:11
 * @Description:
 */
public class DictVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @NotNull(message = "编号不能为空") //不能为空
    private String id;

    /**
     *  编码,所属表字段为t_data_dict_detail.CODE
     */
    @NotNull(message = "编码不能为空") //不能为空
    private String code;
    /**
     *  名称,所属表字段为t_data_dict_detail.NAME
     */
    @Length(min = 3, message = "名称长度错误") //指定长度
    @IsPapName(message = "名称格式错误") //自定义注解校验是否满足手机号格式
    private String name;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
