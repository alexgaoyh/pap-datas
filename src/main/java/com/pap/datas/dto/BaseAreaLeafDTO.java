package com.pap.datas.dto;

import java.io.Serializable;

/**
 * 
 * @author alexgaoyh
 *
 */
public class BaseAreaLeafDTO  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String name;
	
	private String parentId;
	
	private Boolean leaf;
	
	private String level;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Boolean getLeaf() {
		return leaf;
	}

	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return "BaseAreaLeafDTO [id=" + id + ", name=" + name + ", parentId=" + parentId + ", leaf=" + leaf + ", level="
				+ level + "]";
	}
	
}
