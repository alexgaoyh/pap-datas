package com.pap.datas.category.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_data_category", namespace = "com.pap.datas.category.mapper.CategoryMapper", remarks = " 修改点 ", aliasName = "t_data_category t_data_category" )
public class Category extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_data_category.CATEGORY_ID
     */
    @MyBatisColumnAnnotation(name = "CATEGORY_ID", value = "t_data_category_CATEGORY_ID", chineseNote = "编号", tableAlias = "t_data_category", orderNo = 1, methodName = "categoryId")
    @MyApiModelPropertyAnnotation(value = "编号")
    private String categoryId;

    /**
     *  编码,所属表字段为t_data_category.CATEGORY_CODE
     */
    @MyBatisColumnAnnotation(name = "CATEGORY_CODE", value = "t_data_category_CATEGORY_CODE", chineseNote = "编码", tableAlias = "t_data_category", orderNo = 2, methodName = "categoryCode")
    @MyApiModelPropertyAnnotation(value = "编码")
    private String categoryCode;

    /**
     *  名称,所属表字段为t_data_category.CATEGORY_NAME
     */
    @MyBatisColumnAnnotation(name = "CATEGORY_NAME", value = "t_data_category_CATEGORY_NAME", chineseNote = "名称", tableAlias = "t_data_category", orderNo = 3, methodName = "categoryName")
    @MyApiModelPropertyAnnotation(value = "名称")
    private String categoryName;

    /**
     *  备注,所属表字段为t_data_category.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_data_category_REMARK", chineseNote = "备注", tableAlias = "t_data_category", orderNo = 4, methodName = "remark")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    /**
     *  排序号,所属表字段为t_data_category.ORDER_NO
     */
    @MyBatisColumnAnnotation(name = "ORDER_NO", value = "t_data_category_ORDER_NO", chineseNote = "排序号", tableAlias = "t_data_category", orderNo = 5, methodName = "orderNo")
    @MyApiModelPropertyAnnotation(value = "排序号")
    private Integer orderNo;

    /**
     *  类型,所属表字段为t_data_category.CATEGORY_TYPE
     */
    @MyBatisColumnAnnotation(name = "CATEGORY_TYPE", value = "t_data_category_CATEGORY_TYPE", chineseNote = "类型", tableAlias = "t_data_category", orderNo = 6, methodName = "categoryType")
    @MyApiModelPropertyAnnotation(value = "类型")
    private String categoryType;

    private static final long serialVersionUID = 1L;

    @Override
    public String getDynamicTableName() {
        return "t_data_category";
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", categoryId=").append(categoryId);
        sb.append(", categoryCode=").append(categoryCode);
        sb.append(", categoryName=").append(categoryName);
        sb.append(", remark=").append(remark);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", categoryType=").append(categoryType);
        sb.append("]");
        return sb.toString();
    }
}