package com.pap.datas.category.entity;

import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import com.pap.base.mybatis.plugin.annotation.MyBatisTableAnnotation;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_data_category_tree_path", namespace = "com.pap.datas.category.mapper.CategoryTreePathMapper", remarks = " 修改点 ", aliasName = "t_data_category_tree_path t_data_category_tree_path" )
public class CategoryTreePath extends CategoryTreePathKey implements Serializable {
    /**
     *  距离:子代到祖先间隔级数,所属表字段为t_data_category_tree_path.DISTANCE
     */
    @MyBatisColumnAnnotation(name = "DISTANCE", value = "t_data_category_tree_path_DISTANCE", chineseNote = "距离:子代到祖先间隔级数", tableAlias = "t_data_category_tree_path", orderNo = 3, methodName = "distance")
    @ApiModelProperty(value = "距离:子代到祖先间隔级数")
    private Integer distance;

    private static final long serialVersionUID = 1L;

    @Override
    public String getDynamicTableName() {
        return "t_data_category_tree_path";
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", distance=").append(distance);
        sb.append("]");
        return sb.toString();
    }
}