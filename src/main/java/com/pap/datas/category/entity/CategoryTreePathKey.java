package com.pap.datas.category.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class CategoryTreePathKey extends PapBaseEntity implements Serializable {
    /**
     *  祖先:上级节点,所属表字段为t_data_category_tree_path.CATEGORY_ID_ANCESTOR
     */
    @MyBatisColumnAnnotation(name = "CATEGORY_ID_ANCESTOR", value = "t_data_category_tree_path_CATEGORY_ID_ANCESTOR", chineseNote = "祖先:上级节点", tableAlias = "t_data_category_tree_path", orderNo = 1, methodName = "categoryIdAncestor")
    @ApiModelProperty(value = "祖先:上级节点")
    private String categoryIdAncestor;

    /**
     *  子代:下级节点,所属表字段为t_data_category_tree_path.CATEGORY_ID_DESCENDANT
     */
    @MyBatisColumnAnnotation(name = "CATEGORY_ID_DESCENDANT", value = "t_data_category_tree_path_CATEGORY_ID_DESCENDANT", chineseNote = "子代:下级节点", tableAlias = "t_data_category_tree_path", orderNo = 2, methodName = "categoryIdDescendant")
    @ApiModelProperty(value = "子代:下级节点")
    private String categoryIdDescendant;

    private static final long serialVersionUID = 1L;

    @Override
    public String getDynamicTableName() {
        return "t_data_category_tree_path";
    }

    public String getCategoryIdAncestor() {
        return categoryIdAncestor;
    }

    public void setCategoryIdAncestor(String categoryIdAncestor) {
        this.categoryIdAncestor = categoryIdAncestor;
    }

    public String getCategoryIdDescendant() {
        return categoryIdDescendant;
    }

    public void setCategoryIdDescendant(String categoryIdDescendant) {
        this.categoryIdDescendant = categoryIdDescendant;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", categoryIdAncestor=").append(categoryIdAncestor);
        sb.append(", categoryIdDescendant=").append(categoryIdDescendant);
        sb.append("]");
        return sb.toString();
    }
}