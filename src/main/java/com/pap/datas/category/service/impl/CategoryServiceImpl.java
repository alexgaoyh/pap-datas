package com.pap.datas.category.service.impl;

import com.pap.base.exception.PapBusinessException;
import com.pap.base.exception.enums.PapBusinessErrorEnum;
import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.datas.category.entity.Category;
import com.pap.datas.category.entity.CategoryTreePath;
import com.pap.datas.category.mapper.CategoryMapper;
import com.pap.datas.category.mapper.CategoryTreePathMapper;
import com.pap.datas.category.service.ICategoryService;
import com.pap.datas.dto.CategoryWithParentIdAndIdsDTO;
import com.pap.datas.dto.CategoryWithParentIdDTO;
import com.pap.datas.dto.CategoryWithPathsDTO;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("categoryService")
public class CategoryServiceImpl implements ICategoryService {

    @Resource(name = "categoryMapper")
    private CategoryMapper mapper;

    @Resource(name = "categoryTreePathMapper")
    private CategoryTreePathMapper categoryTreePathMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<Category> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(Category record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(Category record) {
       return mapper.insertSelective(record);
    }

    @Override
    public Category selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Category record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Category record) {
      return mapper.updateByPrimaryKey(record);
    }

    // alexgaoyh added begin
    // https://www.percona.com/blog/2011/02/14/moving-subtrees-in-closure-table/

    @Override
    public int insertSelectiveWithPath(CategoryWithParentIdDTO categoryWithParentIdDTO, LoginedUserVO loginedUserVO) {
        Category category = new Category();
        BeanCopyUtilss.myCopyProperties(categoryWithParentIdDTO, category, loginedUserVO);
        mapper.insertSelective(category);

        // 子节点处理： 复制父节点的所有记录，并把这些记录的distance+1，因为子节点到每个上级节点的距离都比它的父节点多一。当然descendant也要改成自己的
        String categoryParentId = categoryWithParentIdDTO.getCategoryParentId();
        if(StringUtils.isNotEmpty(categoryParentId)) {
            categoryTreePathMapper.insertNewNodeByParentIdWithId(categoryWithParentIdDTO.getCategoryId(), categoryWithParentIdDTO.getCategoryParentId());
        }

        // 插入自身节点
        CategoryTreePath categoryTreePath = new CategoryTreePath();
        categoryTreePath.setCategoryIdAncestor(categoryWithParentIdDTO.getCategoryId());
        categoryTreePath.setCategoryIdDescendant(categoryWithParentIdDTO.getCategoryId());
        categoryTreePath.setDistance(0);
        categoryTreePathMapper.insert(categoryTreePath);


        return 1;
    }

    /**
     *
     * https://www.percona.com/blog/2011/02/14/moving-subtrees-in-closure-table/
     *
     * 更新操作的过程中，很难处理树形结构的上下级关系数据，可以使用先删除后插入的方式进行处理。
     * 1、删除操作，需要注意是对当前更新节点(包括当前节点的子节点当做是一个整体进行看待)进行操作，删除此节点与其他节点的关系，注意的是当前节点子树的关系并不能进行删除，
     *    大体的 SQL 语句如下：  比如想要移动节点 D ，那么第一步删除 与 D 节点相关联的关联关系，但是 D 节点下属节点的相互关系并不能进行删除，避免树形结构发生变化。
     *
     *    确保不会删除所有祖先为 D 的子树节点路径
     *
     *      DELETE FROM TreePaths
     *          WHERE descendant IN (SELECT descendant FROM TreePaths WHERE ancestor = 'D')
     *          AND ancestor NOT IN (SELECT descendant FROM TreePaths WHERE ancestor = 'D');
     *
     *      DELETE a FROM TreePaths AS a
     *          JOIN TreePaths AS d ON a.descendant = d.descendant
     *          LEFT JOIN TreePaths AS x
     *          ON x.ancestor = d.ancestor AND x.descendant = a.ancestor
     *          WHERE d.ancestor = 'D' AND x.ancestor IS NULL;
     *
     * 2、插入节点
     *      INSERT INTO TreePaths (ancestor, descendant, length)
     *          SELECT supertree.ancestor, subtree.descendant,
     *          supertree.length+subtree.length+1
     *          FROM TreePaths AS supertree JOIN TreePaths AS subtree
     *          WHERE subtree.ancestor = 'D'
     *          AND supertree.descendant = 'B';
     * @param categoryWithParentIdDTO
     * @param loginedUserVO
     * @return
     */
    @Override
    public int updateSelectiveWithPath(CategoryWithParentIdDTO categoryWithParentIdDTO, LoginedUserVO loginedUserVO) {

        // 优先检查更新操作的过程中，是否会引起自身循环关联（形成一颗环形结构），判断是否引起环形操作的方式为： 祖先节点、子孙节点 之间有没有连线
        Integer distance = categoryTreePathMapper.selectDistanceByAncestorIdAndDescendantId(categoryWithParentIdDTO.getCategoryId(),
                categoryWithParentIdDTO.getCategoryParentId());
        if(distance != null && distance >= 0) {
            throw new PapBusinessException(PapBusinessErrorEnum.CIRCULAR_DEPENDENCY);
        }

        Category category = new Category();
        BeanCopyUtilss.myCopyProperties(categoryWithParentIdDTO, category, loginedUserVO, false);
        mapper.updateByPrimaryKeySelective(category);

        // 删除节点， 把节点 categoryDTO.getCategoryId() 与父类的关系都给删除掉
        categoryTreePathMapper.deleteNodeByCategoryId(categoryWithParentIdDTO.getCategoryId());
        // 插入节点， 把节点 categoryDTO.getCategoryId() 插入到 categoryDTO.getCategoryParentId() 下面
        categoryTreePathMapper.reinsertNodeByCategoryIdAndCategoryParentId(categoryWithParentIdDTO.getCategoryId(), categoryWithParentIdDTO.getCategoryParentId());

        return 1;
    }

    @Override
    public List<CategoryWithPathsDTO> selectAllListWithPaths(String clientLicenseId) {
        return mapper.selectAllListWithPaths(clientLicenseId);
    }

    @Override
    public List<CategoryWithParentIdAndIdsDTO> selectAllListWithParentIdAndIds(String clientLicenseId) {
        return mapper.selectAllListWithParentIdAndIds(clientLicenseId);
    }

    @Override
    public CategoryWithParentIdAndIdsDTO selectObjWithParentIdAndIdsByCategoryId(String categoryId, String clientLicenseId) {
        return mapper.selectObjWithParentIdAndIdsByCategoryId(categoryId, clientLicenseId);
    }

    @Override
    public int deleteByCategoryId(String categoryId, String clientLicenseId) {
        int descendantCount = categoryTreePathMapper.selectCountWithDistinctDescendantByAncestorId(categoryId);
        if(descendantCount > 0) {
            throw new PapBusinessException(PapBusinessErrorEnum.DEPENDENCY_OBJ);
        }
        categoryTreePathMapper.deleteByCategoryIdDescendant(categoryId);
        mapper.deleteByPrimaryKey(categoryId);
        return 0;
    }

}