package com.pap.datas.category.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.datas.category.entity.CategoryTreePath;
import com.pap.datas.category.mapper.CategoryTreePathMapper;
import com.pap.datas.category.service.ICategoryTreePathService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("categoryTreePathService")
public class CategoryTreePathServiceImpl implements ICategoryTreePathService {

    @Resource(name = "categoryTreePathMapper")
    private CategoryTreePathMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<CategoryTreePath> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(CategoryTreePath record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(CategoryTreePath record) {
       return mapper.insertSelective(record);
    }

    @Override
    public CategoryTreePath selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(CategoryTreePath record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(CategoryTreePath record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public int selectCountWithDistinctDescendantByAncestorId(String categoryIdAncestor) {
        return mapper.selectCountWithDistinctDescendantByAncestorId(categoryIdAncestor);
    }

    @Override
    public int deleteByCategoryIdDescendant(String categoryIdDescendant) {
        return mapper.deleteByCategoryIdDescendant(categoryIdDescendant);
    }
}