package com.pap.datas.category.service;

import com.pap.datas.category.entity.CategoryTreePath;

import java.util.List;
import java.util.Map;

public interface ICategoryTreePathService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<CategoryTreePath> selectListByMap(Map<Object, Object> map);

    int insert(CategoryTreePath record);

    int insertSelective(CategoryTreePath record);

    CategoryTreePath selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CategoryTreePath record);

    int updateByPrimaryKey(CategoryTreePath record);

    // alexgaoy added begin

    /**
     * 检查子节点数量
     * 根据 祖先 节点，查询不包含自身的 去重后下级节点 数量
     *
     * @param categoryIdAncestor
     * @return
     */
    int selectCountWithDistinctDescendantByAncestorId(String categoryIdAncestor);

    /**
     * 删除所有节点到当前操作节点的连接关系
     * @param categoryIdDescendant
     * @return
     */
    int deleteByCategoryIdDescendant(String categoryIdDescendant);

}
