package com.pap.datas.category.service;

import com.pap.datas.category.entity.Category;
import com.pap.datas.dto.CategoryWithParentIdAndIdsDTO;
import com.pap.datas.dto.CategoryWithParentIdDTO;
import com.pap.datas.dto.CategoryWithPathsDTO;
import com.pap.obj.vo.logineduser.LoginedUserVO;

import java.util.List;
import java.util.Map;

public interface ICategoryService {

    @Deprecated
    int deleteByPrimaryKey(String id);

    @Deprecated
    int selectCountByMap(Map<Object, Object> map);

    @Deprecated
    List<Category> selectListByMap(Map<Object, Object> map);

    @Deprecated
    int insert(Category record);

    @Deprecated
    int insertSelective(Category record);

    @Deprecated
    Category selectByPrimaryKey(String id);

    @Deprecated
    int updateByPrimaryKeySelective(Category record);

    @Deprecated
    int updateByPrimaryKey(Category record);

    // alexgaoyh added begin

    /**
     * 插入数据，同时维护树形路径。
     * @param categoryWithParentIdDTO
     * @param loginedUserVO
     * @return
     */
    int insertSelectiveWithPath(CategoryWithParentIdDTO categoryWithParentIdDTO, LoginedUserVO loginedUserVO);

    /**
     * 更新数据，同时维护树形路径。
     * @param categoryWithParentIdDTO
     * @param loginedUserVO
     * @return
     */
    int updateSelectiveWithPath(CategoryWithParentIdDTO categoryWithParentIdDTO, LoginedUserVO loginedUserVO);

    /**
     * 查询所有节点，其中每个节点包含从根节点开始的路径
     * @return
     */
    List<CategoryWithPathsDTO> selectAllListWithPaths(String clientLicenseId);

    /**
     * 查询所有节点，其中每个节点包含从根节点开始的路径和父节点编号
     * @return
     */
    List<CategoryWithParentIdAndIdsDTO> selectAllListWithParentIdAndIds(String clientLicenseId);

    /**
     * 查询所有节点，其中每个节点包含从根节点开始的路径
     * @return
     */
    CategoryWithParentIdAndIdsDTO selectObjWithParentIdAndIdsByCategoryId(String categoryId, String clientLicenseId);

    /**
     * 删除当前节点： 仅支持末端节点
     *  1、 删除所有连接点当前节点的连接关系
     *  2、 删除实体信息
     * @param categoryId
     * @return
     */
    int deleteByCategoryId(String categoryId, String clientLicenseId);
}
