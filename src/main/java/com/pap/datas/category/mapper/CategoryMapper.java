package com.pap.datas.category.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.datas.category.entity.Category;
import com.pap.datas.dto.CategoryWithParentIdAndIdsDTO;
import com.pap.datas.dto.CategoryWithPathsDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

;

public interface CategoryMapper extends PapBaseMapper<Category> {
    int deleteByPrimaryKey(String categoryId);

    int selectCountByMap(Map<Object, Object> map);

    List<Category> selectListByMap(Map<Object, Object> map);

    Category selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(Category record);

    int insertSelective(Category record);

    Category selectByPrimaryKey(String categoryId);

    int updateByPrimaryKeySelective(Category record);

    int updateByPrimaryKey(Category record);

    // alexgaoyh added begin

    /**
     * 查询所有节点，其中每个节点包含从根节点开始的路径
     * @return
     */
    List<CategoryWithPathsDTO> selectAllListWithPaths(@Param("clientLicenseId") String clientLicenseId);

    /**
     * 查询所有节点，其中每个节点包含从根节点开始的路径和父节点编号
     * @return
     */
    List<CategoryWithParentIdAndIdsDTO> selectAllListWithParentIdAndIds(@Param("clientLicenseId") String clientLicenseId);

    /**
     * 查询所有节点，其中每个节点包含从根节点开始的路径和父节点编号
     * @return
     */
    CategoryWithParentIdAndIdsDTO selectObjWithParentIdAndIdsByCategoryId(@Param("categoryId") String categoryId,
            @Param("clientLicenseId") String clientLicenseId);

}