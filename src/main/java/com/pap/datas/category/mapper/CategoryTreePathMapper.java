package com.pap.datas.category.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.datas.category.entity.CategoryTreePath;
import com.pap.datas.category.entity.CategoryTreePathKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CategoryTreePathMapper extends PapBaseMapper<CategoryTreePath> {
    int deleteByPrimaryKey(CategoryTreePathKey key);

    int selectCountByMap(Map<Object, Object> map);

    List<CategoryTreePath> selectListByMap(Map<Object, Object> map);

    int insert(CategoryTreePath record);

    int insertSelective(CategoryTreePath record);

    CategoryTreePath selectByPrimaryKey(CategoryTreePathKey key);

    int updateByPrimaryKeySelective(CategoryTreePath record);

    int updateByPrimaryKey(CategoryTreePath record);

    // alexgaoyh added begin

    /**
     *  插入新节点：
     *  当一个节点插入到某个父节点下方时，它将具有与父节点相似的路径，然后再加上一个自身连接即可
     *  所以插入操作需要两条语句，第一条复制父节点的所有记录，并把这些记录的distance加一，因为子节点到每个上级节点的距离都比它的父节点多一。当然descendant也要改成自己的。
     *  例如把id为10的节点插入到id为5的节点下方（这里用了MySQL的语言）
     *  INSERT INTO CategoryTree(ancestor,descendant,distance) (SELECT ancestor,10,distance+1 FROM CategoryTree WHERE descendant=5)
     *
     *  然后就是加入自身连接的记录。
     *  INSERT INTO CategoryTree(ancestor,descendant,distance) VALUES(10,10,0)
     *
     *  insert into select 会造成锁表，这里可以进行优化 可以通过对 select 后面的 where 条件查询语句进行 索引，避免全表扫描
     *  建议针对 CATEGORY_ID_DESCENDANT 添加索引
     * @param categoryId
     * @param categoryParentId
     * @return
     */
    int insertNewNodeByParentIdWithId(@Param("categoryId") String categoryId,
                                      @Param("categoryParentId") String categoryParentId);


    /**
     * 删除当前节点的路径关系
     * https://www.percona.com/blog/2011/02/14/moving-subtrees-in-closure-table/
     * @param categoryId
     * @return
     */
    int deleteNodeByCategoryId(@Param("categoryId") String categoryId);


    /**
     * 重新插入节点的路径关系
     * https://www.percona.com/blog/2011/02/14/moving-subtrees-in-closure-table/
     *
     *  insert into select 会造成锁表，这里可以进行优化 可以通过对 select 后面的 where 条件查询语句进行 索引，避免全表扫描
     *  建议针对 CATEGORY_ID_DESCENDANT 添加索引
     * @param categoryId
     * @param categoryParentId
     * @return
     */
    int reinsertNodeByCategoryIdAndCategoryParentId(@Param("categoryId") String categoryId,
                                                    @Param("categoryParentId") String categoryParentId);

    /**
     * 检查子节点数量
     * 根据 祖先 节点，查询不包含自身的 去重后下级节点 数量
     *
     * @param categoryIdAncestorId
     * @return
     */
    Integer selectCountWithDistinctDescendantByAncestorId(@Param("categoryIdAncestorId") String categoryIdAncestorId);

    /**
     * 删除所有节点到当前操作节点的连接关系
     * @param categoryIdDescendant
     * @return
     */
    int deleteByCategoryIdDescendant(@Param("categoryIdDescendant") String categoryIdDescendant);

    /**
     * 查询 祖先节点、子孙节点 之间的路径长度。
     * @param categoryIdAncestorId
     * @param categoryIdDescendantId
     * @return
     */
    Integer selectDistanceByAncestorIdAndDescendantId(@Param("categoryIdAncestorId") String categoryIdAncestorId,
                                                  @Param("categoryIdDescendantId") String categoryIdDescendantId);

}