package com.pap.datas.category.controller;

import com.pap.beans.idworker.IdWorker;
import com.pap.datas.category.service.ICategoryService;
import com.pap.datas.category.service.ICategoryTreePathService;
import com.pap.datas.dto.CategoryWithParentIdAndIdsDTO;
import com.pap.datas.dto.CategoryWithParentIdAndIdsTreeDTO;
import com.pap.datas.dto.CategoryWithParentIdDTO;
import com.pap.datas.dto.CategoryWithPathsDTO;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/category")
@Api(value = "", tags = "", description = "")
public class CategoryController {

    private static Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @Resource(name = "categoryService")
    private ICategoryService categoryService;

    @Resource(name = "categoryTreePathService")
    private ICategoryTreePathService categoryTreePathService;

    @Resource(name = "idWorker")
    private IdWorker idWorker;

    // https://kyle.ai/blog/6905.html
    // https://www.percona.com/blog/2011/02/14/moving-subtrees-in-closure-table/
    // https://planet.mysql.com/entry/?id=27321
    @ApiOperation("创建")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "categoryWithParentIdDTO", value = "", required = true, dataType = "CategoryWithParentIdDTO", paramType = "body")
    })
    @PostMapping(value = "")
    public ResponseVO<CategoryWithParentIdDTO> categoryCreate(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                                      @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                                      @RequestBody CategoryWithParentIdDTO categoryWithParentIdDTO,
                                                      @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {
        String idStr = idWorker.nextIdStr();
        // 设置主键
        categoryWithParentIdDTO.setCategoryId(idStr);

        // 插入
        int i = categoryService.insertSelectiveWithPath(categoryWithParentIdDTO, loginedUserVO);

        return ResponseVO.successdata(categoryWithParentIdDTO);
    }

    @ApiOperation("更新")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "categoryWithParentIdDTO", value = "", required = true, dataType = "CategoryWithParentIdDTO", paramType = "body")
    })
    @PutMapping(value = "")
    public ResponseVO<CategoryWithParentIdDTO> categoryUpdate(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                                      @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                                      @RequestBody CategoryWithParentIdDTO categoryWithParentIdDTO,
                                                      @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {

        int i = categoryService.updateSelectiveWithPath(categoryWithParentIdDTO, loginedUserVO);


        return ResponseVO.successdata(categoryWithParentIdDTO);
    }

    @ApiOperation("获取所有节点,包含从根节点开始的路径")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String")
    })
    @GetMapping(value = "/listwithpaths")
    public ResponseVO<List<CategoryWithPathsDTO>> categorySelectListWithPaths(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                                                      @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                                                      @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {

        List<CategoryWithPathsDTO> categoryWithPathsDTOList = categoryService.selectAllListWithPaths(loginedUserVO.getClientLicenseId());

        return ResponseVO.successdatas(categoryWithPathsDTOList, null);
    }

    @ApiOperation("获取所有节点,包含从根节点开始的路径和父节点编号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String")
    })
    @GetMapping(value = "/listwithparentidandids")
    public ResponseVO<List<CategoryWithParentIdAndIdsDTO>> categorySelectListWithParentIdAndIds(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                                                                        @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                                                                        @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {

        List<CategoryWithParentIdAndIdsDTO> categoryWithPathsDTOList = categoryService.selectAllListWithParentIdAndIds(loginedUserVO.getClientLicenseId());

        return ResponseVO.successdatas(categoryWithPathsDTOList, null);
    }

    @ApiOperation("获取树形结构")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String")
    })
    @GetMapping(value = "/treewithparentidandids")
    public ResponseVO<List<CategoryWithParentIdAndIdsDTO>> categorySelectTreeListWithParentIdAndIds(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                                                                            @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                                                                            @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {

        List<CategoryWithParentIdAndIdsDTO> categoryWithPathsDTOList = categoryService.selectAllListWithParentIdAndIds(loginedUserVO.getClientLicenseId());

        List<CategoryWithParentIdAndIdsTreeDTO> categoryWithParentIdAndIdsTreeDTOList = CategoryWithParentIdAndIdsTreeDTO.revertToTree(categoryWithPathsDTOList, "-1");
        return ResponseVO.successdatas(categoryWithParentIdAndIdsTreeDTOList, null);
    }

    @ApiOperation("删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
            @ApiImplicitParam(name = "categoryWithPathsDTO", value = "", required = true, dataType = "CategoryWithPathsDTO", paramType = "body")
    })
    @DeleteMapping(value = "")
    public ResponseVO<String> categoryDelete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
                                     @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
                                     @RequestBody CategoryWithPathsDTO categoryWithPathsDTO,
                                     @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {
        CategoryWithParentIdAndIdsDTO categoryWithParentIdAndIdsDTO =
                categoryService.selectObjWithParentIdAndIdsByCategoryId(categoryWithPathsDTO.getCategoryId(), loginedUserVO.getClientLicenseId());

        if(categoryWithParentIdAndIdsDTO != null && StringUtils.isNotEmpty(categoryWithParentIdAndIdsDTO.getCategoryId())) {
            categoryService.deleteByCategoryId(categoryWithParentIdAndIdsDTO.getCategoryId(), loginedUserVO.getClientLicenseId());
        } else {
            return ResponseVO.validfail("未查询到信息,请检查数据正确性");
        }
        return ResponseVO.successdata("操作成功");
    }


//	@ApiOperation("创建")
//	@ApiImplicitParams({
//		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "categoryDTO", value = "", required = true, dataType = "CategoryDTO", paramType="body")
//	})
//	@PostMapping(value = "")
//	public ResponseVO<CategoryDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
//										  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
//										  @RequestBody CategoryDTO categoryDTO,
//										  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
//		String idStr = idWorker.nextIdStr();
//		// 设置主键
//		categoryDTO.setCategoryId(idStr);
//
//		Category databaseObj = new Category();
//		BeanCopyUtilss.myCopyProperties(categoryDTO, databaseObj, loginedUserVO);
//
//		int i = categoryService.insertSelective(databaseObj);
//
//		return ResponseVO.successdata(categoryDTO);
//	}
//
//	@ApiOperation("更新")
//	@ApiImplicitParams({
//		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "categoryDTO", value = "", required = true, dataType = "CategoryDTO", paramType="body")
//	})
//	@PutMapping(value = "")
//	public ResponseVO<CategoryDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
//													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
//													@RequestBody CategoryDTO categoryDTO,
//													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
//
//		Category databaseObj = new Category();
//		BeanCopyUtilss.myCopyProperties(categoryDTO, databaseObj, loginedUserVO, false);
//		int i = categoryService.updateByPrimaryKeySelective(databaseObj);
//
//		// 主键部分
//		databaseObj = categoryService.selectByPrimaryKey(databaseObj.getCategoryId());
//
//		return ResponseVO.successdata(databaseObj);
//	}
//
//
//	@ApiOperation("删除")
//	@ApiImplicitParams({
//		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "categoryDTO", value = "", required = true, dataType = "CategoryDTO", paramType="body")
//	})
//	@DeleteMapping(value = "")
//	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
//													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
//													@RequestBody CategoryDTO categoryDTO,
//													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
//
//		Category databaseObj = new Category();
//		BeanCopyUtilss.myCopyProperties(categoryDTO, databaseObj);
//
//		// 主键部分
//		int i = categoryService.deleteByPrimaryKey(databaseObj.getCategoryId());
//
//		return ResponseVO.successdata("操作成功");
//	}
//
//
//	@ApiOperation("批量删除")
//	@ApiImplicitParams({
//		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "deleteIdsDTO", value = "", required = true, dataType = "DeleteIdsDTO", paramType="body")
//	})
//	@DeleteMapping(value = "/batch")
//	public ResponseVO<String> batch(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
//			@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
//			@RequestBody DeleteIdsDTO deleteIdsDTO,
//			@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {
//		if(deleteIdsDTO != null && StringUtilss.isNotEmpty(deleteIdsDTO.getIds())) {
//			String[] idArray = deleteIdsDTO.getIds().split(",");
//			for(int i = 0; i < idArray.length; i++) {
//				categoryService.deleteByPrimaryKey(idArray[i]);
//			}
//		}
//
//		return ResponseVO.successdata("操作成功");
//	}
//
//	@ApiOperation("详情")
//	@ApiImplicitParams({
//		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
//	})
//	@GetMapping(value = "/{id}")
//	public ResponseVO<CategoryDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
//													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
//													@PathVariable("id") String id){
//
//		Category databaseObj = categoryService.selectByPrimaryKey(id);
//
//		return ResponseVO.successdata(databaseObj);
//	}
//
//	@ApiOperation("查询")
//	@ApiImplicitParams({
//		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
//		@ApiImplicitParam(name = "categoryDTO", value = "应用查询参数", required = false, dataType = "CategoryDTO", paramType="body"),
//		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
//		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
//		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
//		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
//	})
//	@PostMapping(value = "/query")
//	public ResponseVO<CategoryDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
//												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
//												@RequestBody CategoryDTO categoryDTO,
//												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
//												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
//												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
//		Map<Object, Object> map = BeanCopyUtilss.beanToMap(categoryDTO, loginedUserVO);
//		if(pageSize != 0) {
//			Page newPage = new Page(pageNo + "", pageSize + "");
//			map.put("page", newPage);
//		}
//		List<Category> list = categoryService.selectListByMap(map);
//
//		//
//		List<CategoryDTO> dtoList = toDTO(list);
//
//		if(pageSize != 0) {
//			Page newPage = new Page(pageNo + "", pageSize + "");
//			int countNum = categoryService.selectCountByMap(map);
//			newPage.setCount(countNum);
//			return ResponseVO.successdatas(dtoList, newPage);
//		} else {
//			return ResponseVO.successdatas(dtoList, null);
//		}
//
//	}
//
//	private List<CategoryDTO> toDTO(List<Category> databaseList) {
//		List<CategoryDTO> returnList = new ArrayList<CategoryDTO>();
//		if(databaseList != null) {
//			for(Category dbEntity : databaseList) {
//				CategoryDTO dtoTemp = new CategoryDTO();
//				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
//				returnList.add(dtoTemp);
//			}
//		}
//		return returnList;
//	}
//
//	private List<Category> toDB(List<CategoryDTO> dtoList) {
//		List<Category> returnList = new ArrayList<Category>();
//		if(dtoList != null) {
//			for(CategoryDTO dtoEntity : dtoList) {
//				Category dbTemp = new Category();
//				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
//				returnList.add(dbTemp);
//			}
//		}
//		return returnList;
//	}

}
