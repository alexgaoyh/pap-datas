package com.pap.datas.properties.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.datas.properties.entity.DataProperties;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface DataPropertiesMapper extends PapBaseMapper<DataProperties> {
    int deleteByPrimaryKey(String dataPropertiesId);

    int selectCountByMap(Map<Object, Object> map);

    List<DataProperties> selectListByMap(Map<Object, Object> map);

    DataProperties selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(DataProperties record);

    int insertSelective(DataProperties record);

    DataProperties selectByPrimaryKey(String dataPropertiesId);

    int updateByPrimaryKeySelective(DataProperties record);

    int updateByPrimaryKey(DataProperties record);

    // alexgaoyh added begin

    /**
     * 根据 code 查询 value
     * @param dataPropertiesCode
     * @return
     */
    String selectDatePropertiesValueByCode(@Param("dataPropertiesCode") String dataPropertiesCode);

}