package com.pap.datas.properties.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.datas.dto.DataPropertiesDTO;
import com.pap.datas.properties.entity.DataProperties;
import com.pap.datas.properties.service.IDataPropertiesService;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/dataproperties")
@Api(value = "", tags = "", description="")
public class DataPropertiesController {

	private static Logger logger  =  LoggerFactory.getLogger(DataPropertiesController.class);
	
	@Resource(name = "dataPropertiesService")
	private IDataPropertiesService dataPropertiesService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataPropertiesDTO", value = "", required = true, dataType = "DataPropertiesDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<DataPropertiesDTO> dataPropertiesCreate(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody DataPropertiesDTO dataPropertiesDTO,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		// TODO 设置主键
		dataPropertiesDTO.setDataPropertiesId(idStr);

		DataProperties databaseObj = new DataProperties();
		BeanCopyUtilss.myCopyProperties(dataPropertiesDTO, databaseObj, loginedUserVO);

		int i = dataPropertiesService.insertSelective(databaseObj);

		return ResponseVO.successdata(dataPropertiesDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataPropertiesDTO", value = "", required = true, dataType = "DataPropertiesDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<DataPropertiesDTO> dataPropertiesUpdate(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataPropertiesDTO dataPropertiesDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		DataProperties databaseObj = new DataProperties();
		BeanCopyUtilss.myCopyProperties(dataPropertiesDTO, databaseObj, loginedUserVO, false);
		int i = dataPropertiesService.updateByPrimaryKeySelective(databaseObj);

		// TODO 主键部分
		databaseObj = dataPropertiesService.selectByPrimaryKey(databaseObj.getDataPropertiesId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataPropertiesDTO", value = "", required = true, dataType = "DataPropertiesDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> dataPropertiesDelete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody DataPropertiesDTO dataPropertiesDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		DataProperties databaseObj = new DataProperties();
		BeanCopyUtilss.myCopyProperties(dataPropertiesDTO, databaseObj);

		// TODO 主键部分
		int i = dataPropertiesService.deleteByPrimaryKey(databaseObj.getDataPropertiesId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<DataPropertiesDTO> dataPropertiesSelectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		DataProperties databaseObj = dataPropertiesService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "dataPropertiesDTO", value = "应用查询参数", required = false, dataType = "DataPropertiesDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<DataPropertiesDTO> dataPropertiesQuery(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody DataPropertiesDTO dataPropertiesDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(dataPropertiesDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<DataProperties> list = dataPropertiesService.selectListByMap(map);

		//
		List<DataPropertiesDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = dataPropertiesService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}


	@ApiOperation("根据类型查询数据对象集合")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "dataPropertiesType", value = "数据参数类型", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "selectListByType/{dataPropertiesType}")
	public ResponseVO<DataPropertiesDTO> dataPropertiesSelectListByType(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												   @PathVariable("dataPropertiesType") String dataPropertiesType){
		Map<Object, Object> dataPropertiesTypeMap = new HashMap<>(1);
		dataPropertiesTypeMap.put("dataPropertiesType", dataPropertiesType);
		List<DataProperties> databaseList = dataPropertiesService.selectListByMap(dataPropertiesTypeMap);
		List<DataPropertiesDTO> dataPropertiesDTOList = toDTO(databaseList);
		return ResponseVO.successdatas(dataPropertiesDTOList, null);
	}

	@ApiOperation("根据类型、数据参数编码查询数据对象")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "dataPropertiesType", value = "数据参数类型", required = true, dataType = "String", paramType="path"),
			@ApiImplicitParam(name = "dataPropertiesCode", value = "数据参数编码", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "selectObjByTypeAndCode/{dataPropertiesType}/{dataPropertiesCode}")
	public ResponseVO<DataPropertiesDTO> dataPropertiesSelectObjByTypeAndCode(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
														  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
														  @PathVariable("dataPropertiesType") String dataPropertiesType,
														  @PathVariable("dataPropertiesCode") String dataPropertiesCode){
		Map<Object, Object> dataPropertiesTypeMap = new HashMap<>(2);
		dataPropertiesTypeMap.put("dataPropertiesType", dataPropertiesType);
		dataPropertiesTypeMap.put("dataPropertiesCode", dataPropertiesCode);
		List<DataProperties> databaseList = dataPropertiesService.selectListByMap(dataPropertiesTypeMap);
		if(databaseList != null && databaseList.size() > 0) {
			DataProperties dataProperties = databaseList.get(0);
			DataPropertiesDTO dataPropertiesDTO = new DataPropertiesDTO();
			BeanCopyUtilss.myCopyProperties(dataProperties, dataPropertiesDTO);
			return ResponseVO.successdata(dataPropertiesDTO);
		} else {
			return ResponseVO.validfail("未查询到数据!");
		}
	}



	private List<DataPropertiesDTO> toDTO(List<DataProperties> databaseList) {
		List<DataPropertiesDTO> returnList = new ArrayList<DataPropertiesDTO>();
		if(databaseList != null) {
			for(DataProperties dbEntity : databaseList) {
				DataPropertiesDTO dtoTemp = new DataPropertiesDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<DataProperties> toDB(List<DataPropertiesDTO> dtoList) {
		List<DataProperties> returnList = new ArrayList<DataProperties>();
		if(dtoList != null) {
			for(DataPropertiesDTO dtoEntity : dtoList) {
				DataProperties dbTemp = new DataProperties();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
