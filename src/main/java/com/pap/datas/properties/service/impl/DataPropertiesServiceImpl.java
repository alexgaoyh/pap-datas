package com.pap.datas.properties.service.impl;

import com.pap.datas.properties.entity.DataProperties;
import com.pap.datas.properties.mapper.DataPropertiesMapper;
import com.pap.datas.properties.service.IDataPropertiesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("dataPropertiesService")
public class DataPropertiesServiceImpl implements IDataPropertiesService {

    @Resource(name = "dataPropertiesMapper")
    private DataPropertiesMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<DataProperties> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(DataProperties record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(DataProperties record) {
       return mapper.insertSelective(record);
    }

    @Override
    public DataProperties selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(DataProperties record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(DataProperties record) {
      return mapper.updateByPrimaryKey(record);
    }
}