package com.pap.datas.properties.service;

import com.pap.datas.properties.entity.DataProperties;

import java.util.List;
import java.util.Map;

public interface IDataPropertiesService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<DataProperties> selectListByMap(Map<Object, Object> map);

    int insert(DataProperties record);

    int insertSelective(DataProperties record);

    DataProperties selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DataProperties record);

    int updateByPrimaryKey(DataProperties record);
}
