/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50724
Source Host           : 127.0.0.1:3306
Source Database       : cf

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2018-12-28 10:30:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_base_file
-- ----------------------------
DROP TABLE IF EXISTS `t_base_file`;
CREATE TABLE `t_base_file` (
  `FILE_ID` varchar(32) NOT NULL COMMENT '文件编号',
  `FILE_CODE` varchar(32) DEFAULT NULL COMMENT '文件编码',
  `FILE_NAME` varchar(32) DEFAULT NULL COMMENT '文件名称',
  `FILE_SYS_ID` varchar(32) DEFAULT NULL COMMENT '文件所属系统(由上传方维护)',
  `FILE_MODULE_ID` varchar(32) DEFAULT NULL COMMENT '文件所属模块(由上传方维护)',
  `FILE_REF_ID` varchar(32) DEFAULT NULL COMMENT '异构系统关联编号(由上传方维护)',
  `FILE_PATH` varchar(500) DEFAULT NULL COMMENT '文件存储路径',
  `FILE_URL` varchar(255) DEFAULT NULL COMMENT '文件访问路径',
  `FILE_TYPE` varchar(255) DEFAULT NULL COMMENT '文件类型(由上传方维护）',
  `FILE_EXTRA_INFO` varchar(500) DEFAULT NULL COMMENT '上传附件过程中传递过来的额外信息(由上传方维护)',
  `DOWNLOAD_TIME` varchar(32) DEFAULT NULL COMMENT '最近一次下载时间',
  `SEQ_NO` int(10) DEFAULT NULL COMMENT '附件顺序(由上传方维护)',
  `CREATE_USER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` varchar(32) DEFAULT NULL COMMENT '创建日期',
  `CREATE_IP` varchar(32) DEFAULT NULL COMMENT '创建人IP地址',
  `MODIFY_USER` varchar(32) DEFAULT NULL COMMENT '修改人',
  `MODIFY_TIME` varchar(32) DEFAULT NULL COMMENT '修改日期',
  `MODIFY_IP` varchar(32) DEFAULT NULL COMMENT '修改人IP',
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for t_data_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_data_dict`;
CREATE TABLE `t_data_dict` (
  `DICT_ID` varchar(38) NOT NULL COMMENT '编号',
  `DICT_CODE` varchar(32) DEFAULT NULL COMMENT '编码',
  `DICT_NAME` varchar(32) DEFAULT NULL COMMENT '名称',
  `REMARK` varchar(1000) DEFAULT NULL COMMENT '备注',
  `ORDER_NO` varchar(32) DEFAULT NULL COMMENT '排序号',
  `DICT_TYPE` varchar(32) DEFAULT NULL COMMENT '字典类型',
  `CREATE_USER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` varchar(32) DEFAULT NULL COMMENT '创建日期',
  `CREATE_IP` varchar(32) DEFAULT NULL COMMENT '创建人IP地址',
  `MODIFY_USER` varchar(32) DEFAULT NULL COMMENT '修改人',
  `MODIFY_TIME` varchar(32) DEFAULT NULL COMMENT '修改日期',
  `MODIFY_IP` varchar(32) DEFAULT NULL COMMENT '修改人IP',
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`DICT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for t_data_dict_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_data_dict_detail`;
CREATE TABLE `t_data_dict_detail` (
  `DICT__DETAIL_ID` varchar(38) NOT NULL COMMENT '编号',
  `DICT__DETAIL_CODE` varchar(32) DEFAULT NULL COMMENT '编码',
  `DICT__DETAIL_NAME` varchar(32) DEFAULT NULL COMMENT '名称',
  `DICT_ID` varchar(38) DEFAULT NULL COMMENT '所属字典编号',
  `NUM_VALUE` varchar(255) DEFAULT NULL COMMENT '数据值',
  `STR_VALUE` varchar(255) DEFAULT NULL COMMENT '字符值',
  `CREATE_USER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` varchar(32) DEFAULT NULL COMMENT '创建日期',
  `CREATE_IP` varchar(32) DEFAULT NULL COMMENT '创建人IP地址',
  `MODIFY_USER` varchar(32) DEFAULT NULL COMMENT '修改人',
  `MODIFY_TIME` varchar(32) DEFAULT NULL COMMENT '修改日期',
  `MODIFY_IP` varchar(32) DEFAULT NULL COMMENT '修改人IP',
  `CLIENT_LICENSE_ID` varchar(32) DEFAULT NULL COMMENT '授权标示',
  PRIMARY KEY (`DICT__DETAIL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
